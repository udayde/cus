let input = request.input;
let config = request.config;
let context = request.context;
let resource = request.resource;
let gatewayUrl = context.gatewayUrl;
let token = context.accessToken;
let callbackUrl = context.callbackUrl;
let tracking_id = context.trackingId;
let workitem_id = context.workitemId;

//COMPLETED, IN_PROGRESS, FAILED, NEED_REVIEW
response.statusCode = 'COMPLETED';
response.data = { };
response.errorMessage ='';
return response;