const input = request.input;
const config = request.config;
const context = request.context;
const resource = request.resource;
const gatewayUrl = context.gatewayUrl;
const token = context.accessToken;
const callbackUrl = context.callbackUrl;
const tracking_id = context.trackingId;
const workitem_id = context.workitemId;

const Client = require( 'ssh2' ).Client;
const streamBuffers = require( 'stream-buffers' );
const mime = require( 'mime-types' );
const rp = require( 'request-promise' );
const fileType = require( 'file-type' );
const sourceDir = input.source;
// '../sadmin/DE_GET/';
const destDir = input.dest;
// '../sadmin/GET_REPOSITORY/';

// Setup Connection
const connSettings = {
  host: 'talend.dynpro.com',
  port: 22222,
  username: 'sadmin',
  password: 'j3e3wrEHX9Vg#',
};
// UUID generator
const generateUid = () => {
  return 'yxxx-xxx'.replace( /[xy]/g, function( c ) {
    const r = Math.random() * 16 | 0;
    const v = c == 'x' ? r : ( r & 0x3 | 0x8 );
    return v.toString( 16 );
  } );
};
// DE environment details
const getEnviormentDetails = () => {
  return {
    baseUri: gatewayUrl,
    fileServer: '/fileserver/file',
  };
};
/**
 * LOG WITH STANDARD MESSAGE PREFIX.
 * @param {string} context The first string.
 * @param {string} message The second string.
 * @return {void}
 */
function logMsg( context, message ) {
  let contextStr = context.toString();
  if ( Array.isArray( context ) ) {
    contextStr = context.join( '|' );
  }

  console.log( `SFTP(${contextStr}): ${message}` );
}
/**
 * Use filename To Determine Content To Be Used.
 * @param {string} filename The first string.
 * @param {string} message The second string.
 * @return {string} The mime type.
 */
function determineContentType( filename ) {
  let ctype = mime.lookup( filename );

  // If Not Known Use Generic MIME Type
  if ( ctype == false ) {
    ctype = 'application/octet-stream';
  }

  return ctype;
}
/**
 * Directory Listing.
 * @param {string} dir The directory string.
 * @return {string} promise resolved listing.
 */
function readDirectory( dir ) {
  const conn = new Client();
  const context = 'READ_DIR';
  return new Promise( function( resolve, reject ) {
    conn.on( 'ready', function() {
      logMsg( [context], 'Authenticated' );
      conn.sftp( function( err, sftp ) {
        if ( err ) {
          logMsg( [context], err );
          reject( err );
          conn.end();
          return;
        }
        logMsg( [context], 'Established' );

        sftp.readdir( dir, function( err, list ) {
          if ( err ) {
            logMsg( [context], err );
            reject( err );
            conn.end();
            return;
          }

          logMsg( [context], `${list.length} files found` );

          conn.end();
          resolve( list );
        } );
      } );
    } ).on( 'end', function() {
      logMsg( [context], 'Disconnected' );
    } ).on( 'close', function() {
      logMsg( [context], 'Closed' );
    } ).on( 'error', function( err ) {
      logMsg( [context], 'Error connecting to server - ' + err );
      reject( err );
    } ).connect( connSettings );
  } );
}

const formatResults = (res) => {
  const fileName = res.name;
  return {
      'id': res.db_id,
      'name': fileName,
      'type': res.contentType
  };
};

const formatTimeStamp = ( timestamp ) => {
  const formattedTimestamp = timestamp.toISOString().replace( /T/, ' ' ).replace( /\..+/, '' ).replace( /-/g, '/' );
  return formattedTimestamp;
};
/**
 * Download File From Server
 * @param {string} dir The directory string.
 * @param {string} filename The file name string.
 * @return {string} resolved filename,contentType,timestamp,contentSize,fullPath,contentBuf.
 */
function downloadFile( dir, filename ) {
  const timestamp =formatTimeStamp(new Date());
  const conn = new Client();
  const contentType = determineContentType( filename );
  const context = 'DOWNLOAD';
  return new Promise( function( resolve, reject ) {
    conn.on( 'ready', function() {
      logMsg( [context, filename], 'Authenticated' );
      conn.sftp( function( err, sftp ) {
        if ( err ) {
          logMsg( [context], err );
          reject( err );
          conn.end();
          return;
        }
        logMsg( [context, filename], 'Established' );

        // Setup Read Stream
        const fullFilePath = `${dir}${filename}`;
        logMsg( [context, filename], fullFilePath );
        const readStream = sftp.createReadStream( fullFilePath );

        // Setup Read Stream Event Handlers
        readStream.on( 'data', ( chunk ) => {
          logMsg( [context, filename], 'Chunk Len: ' + chunk.length );
        } );

        readStream.on( 'error', function() {
          logMsg( [context, filename], 'Transfer Failed - Read' );
          conn.end();
          reject( 'File Transfer FAILED - Read' );
          return;
        } );

        // Setup Write Stream
        const writeStream = new streamBuffers.WritableStreamBuffer( {
          initialSize: ( 100 * 1024 ), // start at 100 kilobytes.
          incrementAmount: ( 10 * 1024 ), // grow by 10 kilobytes each time buffer overflows.
        } );

        // Setup Write Stream Event Handlers
        writeStream.on( 'finish', function() {
          logMsg( [context, filename], 'Transfer Succeeded' );
          conn.end();
          let contentBuf = writeStream.getContents();
          let contentSize = 0;
          if ( contentBuf ) {
            contentSize = contentBuf.length;
          } else {
            contentBuf = Buffer.alloc( 0 );
          }
          resolve( {
            filename: filename,
            contentType: contentType,
            timestamp: timestamp,
            contentSize: contentSize,
            fullPath: fullFilePath,
            contentBuf: contentBuf,
          } );
        } );

        writeStream.on( 'error', function() {
          logMsg( [context, filename], 'Transfer Failed - Write' );
          conn.end();
          reject( 'File Transfer FAILED - Write' );
        } );

        // Initiate File Transfer
        readStream.pipe( writeStream );
      } );
    } ).on( 'end', function() {
      logMsg( [context, filename], 'Disconnected' );
    } ).on( 'close', function() {
      logMsg( [context, filename], 'Closed' );
    } ).on( 'error', function( err ) {
      logMsg( [context, filename], 'Error connecting to server - ' + err );
      reject( err );
    } ).connect( connSettings );
  } );
}

/**
 * Read and download files to buffer
 * @param {string} sourceDir The directory string.
 * @param {string} destDir The directory string.
 * @return {string} Read download response with filesfound boolean and file list if any.
 */
async function readDownloadFiles( sourceDir, destDir ) {
  // Get List Of Files
  logMsg( 'STATUS', '*****LIST DIRECTORY************************************************' );
  dirList = await readDirectory( sourceDir );
  // await sleep( 1000 )
  if ( dirList.length == 0 ) {
    logMsg( 'STATUS', 'Directory Empty' );
  }
  for ( item of dirList ) {
    logMsg( 'STATUS', JSON.stringify( item ) );
  }

  // Download Files - Sync Approach
  logMsg( 'STATUS', '*****DOWNLOAD FILES************************************************' );
  const fileObjList = [];
  let FilesFound;
  let readDownloadResponse = {};
  if ( dirList.length > 0 ) {
    for ( item of dirList ) {
      try {
        const fileObj = await downloadFile( sourceDir, item.filename );
        FilesFound = true;
        fileObjList.push( fileObj );
        readDownloadResponse= {
          'filesFound': FilesFound,
          'fileList': fileObjList,
        };
      } catch ( err ) {
        // Skip File - If Not Able To Download Just Leave In Directory
      }
    }
    await sleep( 1000 );
  } else {
    FilesFound = false;
    readDownloadResponse = {
      'filesFound': FilesFound,
      'fileList': [],
    };
  }

  logMsg( ['STATUS'], 'DONE WITH STUFF' );
  return readDownloadResponse;
}
// Save File to DE
const saveFileToDe = async ( fileObject ) => {
  try {
    const envDetail = getEnviormentDetails();
    const requestUri = envDetail.baseUri + envDetail.fileServer;
    let fileSaveResponseData;
    let responseData = {
      fileObject: fileObject,
    };
    const isArray = Array.isArray( fileObject );
    if ( isArray ) {
      const arrayOfRequestObjects = [];
      fileObject.forEach( ( element, index ) => {
        const currentRequestObject = {};
        currentRequestObject.method = 'POST';
        currentRequestObject.uri = requestUri;
        const currentFileName = generateUid() + '_' + element.filename;
        element.uploadedfilename = currentFileName;
        const fileData = element.contentBuf;
        const formData = {
          'file': {value: fileData, options: {filename: currentFileName, contentType: element.contentType}},
          'fileMeta': {value: JSON.stringify({'bucket': 'Global', 'name': currentFileName}), options: {contentType: 'application/json'}},
        };
        currentRequestObject.formData = formData;
        currentRequestObject.headers = {'Authorization': 'Bearer ' + token};
        currentRequestObject.formData = formData;
        currentRequestObject.strictSSL = false;
        currentRequestObject.headers = {
          'Authorization': 'Bearer ' + token,
        };

        arrayOfRequestObjects.push( currentRequestObject );
      } );
      fileSaveResponseData = await Promise.all( arrayOfRequestObjects.map( async ( elementParam ) => {
        let fileResponse = await rp( elementParam );
        if ( typeof fileResponse === 'string' ) {
          fileResponse = JSON.parse( fileResponse );
        }
        return fileResponse;
      } ) );
    } else {
      const currentRequestObject = {};
      currentRequestObject.method = 'POST';
      currentRequestObject.uri = requestUri;
      const fileData = Buffer.from( fileObject.contentBuf, 'base64');
      const filename = fileObject.filename + '_' + generateUid();
      fileObject.uploadedfilename = filename;
      const formData = {
        'file': {value: fileData, options: {filename: currentFileName, contentType: element.contentType}},
        'fileMeta': {value: JSON.stringify({'bucket': 'Global', 'name': currentFileName}), options: {contentType: 'application/json'}},
      };
      currentRequestObject.formData = formData;
      currentRequestObject.strictSSL = false;
      currentRequestObject.headers = {
        'Authorization': 'Bearer ' + token,
      };

      fileSaveResponseData = await rp(currentRequestObject);
      if ( typeof fileSaveResponseData === 'string' ) {
        fileSaveResponseData = JSON.parse( fileSaveResponseData );
      }
    }
    responseData.fileSaveResponseData = fileSaveResponseData;
    responseData = deCso.cleanUpDeSaveFileResponse( responseData );
    return responseData;
  } catch ( err ) {
    logMsg( 'Bot Error saveFileToDe===========' + err );
  }
};
// Cleanup
const deCso = {};
deCso.cleanUpDeSaveFileResponse = ( responseData ) => {
  const isArray = Array.isArray( responseData.fileObject );
  if ( isArray ) {
    responseData.fileObject.forEach( ( element, index ) => {
      const fileId = responseData.fileSaveResponseData[index].db_id;
      // let responsefilename = responseData.fileSaveResponseData[ index ].filename;
      // if ( element.uploadedfilename == responsefilename ) {
      delete element.file;
      element.fileId = fileId;
      // }
    } );
    responseData = responseData.fileObject;
  } else {
    const fileId = responseData.fileSaveResponseData.db_id;
    const responsefilename = responseData.fileSaveResponseData.name;
    // if ( responseData.fileObject.filename == responsefilename ) {
    delete responseData.fileObject.file;
    responseData.fileObject.fileId = fileId;
    // }
    responseData = responseData.fileObject;
  }
  return responseData;
};
/**
 * Read and download files to buffer
 * @param {string} ms The time constraint.
 * @return {string} promise timeout.
 */
function sleep( ms ) {
  return new Promise( (resolve) => setTimeout( resolve, ms ));
}
const cleanupResponse = async ( filesaveResponse ) => {
  const converted = [];
  for ( i = 0; i < filesaveResponse.length; i++ ) {
    delete filesaveResponse[i].contentBuf;
    console.log( filesaveResponse[i] );
    converted.push( filesaveResponse[i] );
  }

  return converted;
};
const createResponseObject = async (filesfound, savedfiledata)=>{
  if (filesfound==true) {
    response.statusCode = 'COMPLETED';
    response.data = {
      'filesFound': filesfound,
      'saveFileResponse': savedfiledata.savedFileData,
    };
    response.errorMessage = '';
  } else {
    response.statusCode = 'COMPLETED';
    response.data = {
      'filesFound': filesfound,
      'saveFileResponse': [''],
    };
    response.errorMessage = '';
  }
  return response;
};
const mainBotExecution = async () => {
  try {
    const fileObjList = await readDownloadFiles( sourceDir, destDir );
    logMsg( 'STATUS', '*****STORE FILE****************************************************' );
    let filesaveResponseObject ={};
    let response = {}
    if (fileObjList.fileList.length>0) {
      filesaveResponse = await saveFileToDe( fileObjList.fileList );
      await sleep( 1000 );
      cleanedResponse = await cleanupResponse(filesaveResponse);
      filesaveResponseObject = {
        savedFileData: cleanedResponse,
      };

      response = createResponseObject(fileObjList.filesFound, filesaveResponseObject);
    } else {
      filesaveResponseObject = {
        savedFileData: [],
      };

      response = createResponseObject(fileObjList.filesFound, filesaveResponseObject);
    }

    return response;
  } catch ( err ) {
    logMsg( 'ERROR_STUFF', err );
    response.statusCode = 'FAILED';
    response.data = {};
    response.errorMessage = err;
    return response;
  }
};
return mainBotExecution();
