/** utility package objects available to be included as needed:
* lodash https://lodash.com/docs/
* aws-sdk: https://www.npmjs.com/package/aws-sdk
* json2csv: https://www.npmjs.com/package/json2csv
* nunjucks: https://www.npmjs.com/package/nunjucks
* request: https://www.npmjs.com/package/request
* request-promise: https://www.npmjs.com/package/request-promise
* yauzl-promise https://www.npmjs.com/package/yauzl-promise
* ssh2: https://www.npmjs.com/package/ssh2
* stream-buffers: https://www.npmjs.com/package/stream-buffers

* include using require pattern, i.e.
* const _ = require('lodash');
*/

let input = request.input;
let config = request.config;
let context = request.context;
let resource = request.resource;
let gatewayUrl = context.gatewayUrl;
let token = context.accessToken;
let callbackUrl = context.callbackUrl;
let tracking_id = context.trackingId;
let workitem_id = context.workitemId;

//let token = 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJTeXN0ZW1Vc2VyIiwiYXVkIjoiYW55IiwidXNlckRldGFpbCI6IntcInVzZXJuYW1lXCI6XCJTeXN0ZW1Vc2VyXCIsXCJmaXJzdG5hbWVcIjpudWxsLFwibGFzdG5hbWVcIjpudWxsLFwiZW1haWxcIjpudWxsLFwiYXV0aG9yaXRpZXNcIjpbe1wiYXV0aG9yaXR5XCI6XCJST0xFX1NZU1RFTVwifV0sXCJlbmFibGVkXCI6dHJ1ZSxcInN5c3RlbVVzZXJcIjp0cnVlfSIsImlhdCI6MTUxMjM4NTc3NH0.pG9i3Y5baS_i52MmYwdhQJtrrOyZWqqnwzfA8rRgU7QQyjjNCEgEqU1ses2iSQ-ZplYW3BLkUzYobO6vHunu4w'
//let gatewayUrl = 'http://ec2-52-90-88-208.compute-1.amazonaws.com';
//let response = {}

// const fs = require('fs');
const rp = require('request-promise');
const readline = require('readline');
const _ = require('lodash');
const stream = require('stream');
const { promisify } = require('util');

const dataSetName = input.dataSetName;


const handleErrors = (hasError, failProcess, errorDescription) => {
    hasError = hasError;
    failProcess = failProcess;

    if (failProcess == true) {
        console.log("BOT ERROR========================= " + errorDescription);
        return {
            statusCode: 'FAILED',
            errorMessage: errorDescription,
            data: {}
        }
    }
}

//DE environment details
const getEnviormentDetails = () => {
    return {
        baseUri: gatewayUrl,
        deRotatePath: '/DE_ROTATE-1/rotate',
        docClassifyPath: '/DOC_CLASSIFY-1/infer',
        fileServer: '/fileserver/file'
    }
}
let envDetailforocnsole = getEnviormentDetails();
console.log("FOR CONSOLE ENVUIRONMENT DETAILS"+envDetailforocnsole);
const getCombinedIdForAsnId = (currentObject) => {
    let rowIdentifier = currentObject.billOfLading + "-" + currentObject.externalRef;
    return rowIdentifier.trim();
}

const searchForMatchingAsnRecord = async (asnCombinedId) => {
    try {
        let requestObject = {
            "fields": { "data": "true" },
            "filter": { "data.rowIdentifier": `${asnCombinedId}` },
            "sort": {}
        }

        // let searchResponse = await makeApiRequests('datasets/type/standard/instance/bolAsnRecords/search', requestObject, 'POST');
        let searchResponse = await makeApiRequests(`datasets/dataset/${dataSetName}/record/search`, requestObject, 'POST');
        if (searchResponse && searchResponse.content && searchResponse.content.length > 0) {
            return true;
        } else {
            return false;
        }
    } catch (err) {
        handleErrors(true, true, "BOT ERROR getCombinedIdForAsnId =====================> " + err);
    }
}




const generateDatasetObject = async (fileToProcess) => {
    try {

        const rl = readline.createInterface({
            input: await bufferToStream(Buffer.from(fileToProcess, 'base64')),
            crlfDelay: Infinity
        });

        // Get Each Line Of The file

        let arrayOfRows = [];

        rl.on('line', (lineToRead) => {
            arrayOfRows.push(lineToRead);
        });

        rl.on[promisify.custom] = (rlStatus) => {
            return new Promise((resolve) => {
                rl.on(rlStatus, resolve);
            });
        };

        const rlClose = await promisify(rl.on)('close')
        //rlClose.close()


        let numberOfNewRecords = 0;
        let numberOfExistingRecords = 0;
        let newAsnRecords = [];
        for (currentLine in arrayOfRows) {
            let line = arrayOfRows[currentLine]
            let currentAsnObject = {};
            let addFileToAsnDataSet = false;
            // console.log(line)
            //let rl = rl
            let fields = line.split('\t');
            console.log("BOL NUMBER IS=>>>>>>>>>"+fields[14]);
            if (fields[1] == '1' && fields[14]!='') {
                let BolNumber = fields[14].replace(/^0+/, '');
                addFileToAsnDataSet = true;
                currentAsnObject.refDoc = (fields[10] || "").trim();
                currentAsnObject.externalRef = (fields[16] || "").trim();
                currentAsnObject.billOfLading = (BolNumber || "").trim();
                currentAsnObject.searchRefDoc = (fields[10] || "").replace(/\W/g, '');
                currentAsnObject.searchExternalRef = (fields[16] || "").replace(/\W/g, '');
                currentAsnObject.searchBolNumber = (BolNumber || "").replace(/\W/g, '');
                currentAsnObject.rowIdentifier = getCombinedIdForAsnId(currentAsnObject);
                addFileToAsnDataSet = await searchForMatchingAsnRecord(currentAsnObject.rowIdentifier);
                if (!addFileToAsnDataSet) {
                    let wrappedObject = {data: currentAsnObject};
                    newAsnRecords.push(wrappedObject);
                    numberOfNewRecords++;
                } else {
                    numberOfExistingRecords++;
                }
            }
        }

        let asnObjects = {
            "numberOfExistingRecords": numberOfExistingRecords,
            "numberOfNewRecords": numberOfNewRecords,
            "newAsnObjects": newAsnRecords
        };

        return asnObjects;

    } catch (err) {
        handleErrors(true, true, "BOT ERROR generateDatasetObject =====================> " + err);
    }
}

const makeApiRequests = async (endpointUri, data = {}, method) => {
    try {
        const options = {
            method: method,
            uri: `${gatewayUrl}/${endpointUri}`,
            qs: {},
            headers: {
                'Authorization': 'Bearer ' + token,
            },
            rejectUnauthorized: false,
            body: data,
            json: true
        };

        let responseObj = await rp(options);
        return responseObj;
    } catch (err) {
        handleErrors(true, true, "BOT ERROR makeApiRequests =====================> " + err);
    }
}


const getFileFromDe = async (fileObject) => {
    try {
        let envDetail = getEnviormentDetails();
        let requestUri = envDetail.baseUri + envDetail.fileServer;
        let responseData = { fileObject: fileObject };
        let isArray = Array.isArray(fileObject);
        if (isArray) {
            let arrayOfRequestObjects = [];
            fileObject.forEach((element) => {
                let currentRequestObject = {};
                currentRequestObject.headers = { 'Authorization': 'Bearer ' + token };
                currentRequestObject.method = 'GET';
                currentRequestObject.rejectUnauthorized = false;
                currentRequestObject.encoding = null;
                currentRequestObject.uri = requestUri + "/" + element.fileId;
                arrayOfRequestObjects.push(currentRequestObject);
            });

            let fileDownloadResponse = await Promise.all(arrayOfRequestObjects.map(async (elementParam) => {
                let fileResponse = await rp(elementParam);
                let bufferFile = Buffer.from(fileResponse);
                return bufferFile.toString('base64');
            }));

            responseData.downloadedFiles = fileDownloadResponse;

        }
        else {
            let currentRequestObject = {};
            currentRequestObject.headers = { 'Authorization': 'Bearer ' + token };
            currentRequestObject.method = 'GET';
            currentRequestObject.uri = requestUri + "/" + fileObject.fileId;
            currentRequestObject.rejectUnauthorized = false;
            currentRequestObject.encoding = null;
            let fileDownloadResponse = await rp(currentRequestObject);
            let bufferFile = Buffer.from(fileDownloadResponse);
            responseData.downloadedFiles = bufferFile.toString('base64');
        }
        responseData = deCso.cleanUpResponseData(responseData, 'de', 'getFile');
        return responseData;
    }
    catch (err) {
        console.log(err)
        handleErrors(true, true, err);
    }
}

let deCso = {};
deCso.cleanUpResponseData = (responseData, integrationService, action) => {
    try {
        let replacementObjectName = {
            'ms': { '@odata.context': 'odata.context', '@microsoft.graph.downloadUrl': 'microsoft.graph.downloadUrl' },
            's3': { 'downloadCleanUp': { 'Body': 'file', 'fileName': 'fileName', 'ContentType': 'contentType' } },
            'de': {
                'deRotate': { 'rotated_file': 'file', 'filename': 'fileName', 'rotated_contentType': 'contentType', "status": "status" },
                'classify': { 'file': 'file', 'filename': 'fileName', 'contentType': 'contentType', 'predicted_class': 'predicted_class', "status": "classifiedStatus" }
            }
        };
        switch (integrationService) {
            case "msOneDrive":
                if (action == "GETITEMID") {
                    responseData = updateNamingConventions(responseData, replacementObjectName.ms);
                    if (responseData.value) {
                        responseData.value = updateNamingConventions(responseData.value, replacementObjectName.ms);
                    }
                } else { responseData = responseData }
                break;
            case "s3":
                if (action == "S3_getobjects") {
                    responseData = updateNamingConventions(responseData, replacementObjectName.s3.downloadCleanUp);
                }
                break;
            case "de":
                if (action == "deRotate") {
                    responseData.response.status = responseData.status;
                    responseData = updateNamingConventions(responseData.response, replacementObjectName.de.deRotate);
                }
                if (action == "classify") {
                    responseData.response.status = responseData.status;
                    responseData.response.extractionStatus = "NOTSTARTED";
                    responseData = updateNamingConventions(responseData.response, replacementObjectName.de.classify)
                }
                if (action == "storeFile") {
                    let isArray = Array.isArray(responseData.fileObject);
                    if (isArray) {
                        responseData.fileObject.forEach((element, index) => {
                            let fileId = responseData.fileSaveResponseData[index].fileId;
                            let responseFileName = responseData.fileSaveResponseData[index].fileName;
                            if (element.uploadedFileName == responseFileName) {
                                delete element.file;
                                element.fileId = fileId;
                            }
                        });
                        responseData = responseData.fileObject;
                    } else {
                        let fileId = responseData.fileSaveResponseData.fileId;
                        let responseFileName = responseData.filename;
                        if (fileObject.filename == responseFileName) {
                            delete fileObject.file;
                            fileObject.fileId = fileId;
                        }
                    }
                }

                if (action == "getFile") {
                    let isArray = Array.isArray(responseData.fileObject);
                    if (isArray) {
                        responseData.fileObject.forEach((element, index) => {
                            element.file = responseData.downloadedFiles[index];
                        });
                        responseData = responseData.fileObject;
                    } else {
                        responseData.fileObject.file = responseData.downloadedFiles;
                        responseData = responseData.fileObject;
                    }
                }
                break;

            default:
                responseData = responseData;
                break;
        }
        return responseData;
    }
    catch (err) {
        handleErrors(true, true, err)
    }
}

const bufferToStream = async (buffer) => {
    var bufferStream = new stream.PassThrough();
    bufferStream.end(buffer);
    bufferStream.pipe(process.stdout)
    return bufferStream;
}

const readFileAndUpdateAsnDataSet = async (fileObjectToProcess) => {
    try {
        let readAndUpdateResponse = {
            hasAsnRecordsToUpdate: false
        }

        let asnDatasetObject = await generateDatasetObject(fileObjectToProcess.file);
       
        if (asnDatasetObject.numberOfNewRecords > 0) {
            readAndUpdateResponse.hasAsnRecordsToUpdate = true;
            // await makeApiRequests('datasets/type/standard/instance/bolAsnRecords/bulk', asnDatasetObject.newAsnObjects, 'POST');
            await makeApiRequests(`datasets/dataset/${dataSetName}/record/bulk`, asnDatasetObject.newAsnObjects, 'POST');
        }
        if (asnDatasetObject.numberOfExistingRecords > 0) {
            readAndUpdateResponse.existingAsnObjects = asnDatasetObject.existingAsnObjects;
        }

        return readAndUpdateResponse;
    } catch (err) {
        handleErrors(true, true, "BOT ERROR =====readFileAndUpdateAsnDataSet========== " + err);
    }
}






const mainBotExecution = async () => {
    try {
        let fileObjectsToProcess = await getFileFromDe(input.fileObjList);
        console.log("FILE OBJECT LIST"+fileObjectsToProcess);
        let new_asn_added = false;
        let resultingDatasetUpdate = await Promise.all(fileObjectsToProcess.map(async (elementParam) => {
            let singleFileResponse = await readFileAndUpdateAsnDataSet(elementParam);
            elementParam.updateResponse = singleFileResponse;
            if (singleFileResponse && singleFileResponse.hasAsnRecordsToUpdate == true) {
                new_asn_added = true;
            }
             console.log("SINGLE ELEMENT PARAM(CONSOLING FOR TESTING)====>>>>>>>   "+JSON.stringify(elementParam,null,2));
             delete elementParam.file;
            return elementParam;
        }));
            console.log("<<<<<<<<====TOTAL ASN UPDATE DATA (CONSOLING FOR TESTING)====>>>>>>>   "+JSON.stringify(resultingDatasetUpdate,null,2));
        response.statusCode = 'COMPLETED';
        response.data = {
            'updateAsnResults': resultingDatasetUpdate,
            'newAsnAdded': new_asn_added
        };

        response.errorMessage = '';
        return response;
    } catch (err) {
        handleErrors(true, true, "BOT ERROR =====mainBotExecution========== " + err);
        response.statusCode = 'FAILED';
        response.data = '';
        response.errorMessage = err;
        return response;
    }
}



return mainBotExecution();