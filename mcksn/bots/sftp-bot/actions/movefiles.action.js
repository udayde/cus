let input = request.input;
let config = request.config;
let context = request.context;
let resource = request.resource;
let gatewayUrl = context.gatewayUrl;
let token = context.accessToken;
let callbackUrl = context.callbackUrl;
let tracking_id = context.trackingId;
let workitem_id = context.workitemId;

const client = require( 'ssh2' )
const streamBuffers = require( 'stream-buffers' )
const mime = require( 'mime-types' );
const path = require( 'path' );
let sourceDir =input.source;
//  '../sadmin/DE_GET/';
let destDir = input.dest;
// '../sadmin/GET_REPOSITORY/';

let fileObjList = input.saveFileResponse;


// Setup Connection
let connSettings = {
     host: 'talend.dynpro.com',
    port: 22222,
    username: 'sadmin',
    password: 'j3e3wrEHX9Vg#'
}

// LOG WITH STANDARD MESSAGE PREFIX
function logMsg( context, message ) {
    let contextStr = context.toString()
    if ( Array.isArray( context ) ) {
        contextStr = context.join( '|' )
    }

    console.log( `SFTP(${contextStr}): ${message}` )
}

function moveFile( sDir, dDir, filename ) {
    const conn = new client()
    let context = 'MOVE_FILE'
    return new Promise( function ( resolve, reject ) {
        conn.on( 'ready', function () {
            logMsg( [ context, filename ], 'Authenticated' )
            conn.sftp( function ( err, sftp ) {
                if ( err ) {
                    logMsg( [ context, filename ], err )
                    reject( err )
                    conn.end()
                    return
                }
                logMsg( [ context, filename ], 'Established' )

                let fullFilePathSource = `${sDir}${filename}`
                let fullFilePathDest = `${dDir}${filename}`
                logMsg( [ context, filename ], `${sDir} to ${dDir}` )
                sftp.rename( fullFilePathSource, fullFilePathDest, function ( err ) {
                    if ( err ) {
                        logMsg( [ context, filename ], err )
                        reject( err )
                        conn.end()
                        return
                    }

                    logMsg( [ context, filename ], 'File Moved' )

                    conn.end()
                    resolve()
                } )
            } )
        } ).on( 'end', function () {
            logMsg( [ context, filename ], 'Disconnected' )
        } ).on( 'close', function () {
            logMsg( [ context, filename ], 'Closed' )
        } ).on( 'error', function ( err ) {
            logMsg( [ context, filename ], 'Error connecting to server - ' + err )
            reject( err )
        } ).connect( connSettings )
    } )
}
async function moveFilesSourceToDest( sourceDir, destDir ) {
    // Archives File On Server
    logMsg( 'STATUS', '*****ARCHIVE FILES**************************************************' )
   
    if (input.filesFound == true) {
         let fileObjMovedList = [];
        for (item of fileObjList) {
            try {
                await moveFile(sourceDir, destDir, item.filename)
                fileObjMovedList.push(item)
            } catch (err) {
                // Skip File - If Not Able To Move Just Leave In Directory
                logMsg(['STATUS', 'ERR'], 'MOVE ERROR')
            }
        }
        fileObjList.movedFileList = fileObjMovedList;
        logMsg( [ 'STATUS' ], 'Files Available and Moved' )
        await sleep( 1000 )
        return fileObjList;
    }
    else{
        //fileobjList.movedFileList = fileObjMovedList;
    logMsg( [ 'STATUS' ], 'No File Available' )
    return "No files Found";
    } 
}

function sleep( ms ) {
    return new Promise( resolve => setTimeout( resolve, ms ) )
}
const mainBotExecution = async () => {
    try {
        movedFilesResponse = await moveFilesSourceToDest( sourceDir, destDir );
        //COMPLETED, IN_PROGRESS, FAILED, NEED_REVIEW
        response.statusCode = 'COMPLETED';
        response.data = {
            filesFound:input.filesFound,
            fileObjList: movedFilesResponse
            };
        response.errorMessage ='';
        logMsg( 'STATUS', 'Completed moving files' )
        return response;   
    } catch ( err ) {
        logMsg( 'ERROR_moveFilesSourceToDest', err )
    }

}
return mainBotExecution();