exports.mockRequest = {
    context: {
      gatewayUrl: 'https://22.dev.decisionengines.ai',
      accessToken: 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1YmVydXNlciIsImF1ZCI6ImFueSIsInVzZXJEZXRhaWwiOiJ7XCJ1c2VybmFtZVwiOlwidWJlcnVzZXJcIixcImZpcnN0bmFtZVwiOlwidWJlcnVzZXJcIixcImxhc3RuYW1lXCI6XCJ1YmVyVXNlck5hbWVcIixcImVtYWlsXCI6bnVsbCxcImF1dGhvcml0aWVzXCI6W3tcImF1dGhvcml0eVwiOlwiUk9MRV9VU0VSXCJ9LHtcImF1dGhvcml0eVwiOlwicm9sZTo1ZGVkZmNmYWNkNzEwYTAwMDg2YmVjNDRcIn0se1wiYXV0aG9yaXR5XCI6XCJyb2xlOjFcIn1dLFwiZW5hYmxlZFwiOnRydWUsXCJ0ZW5hbnRJZFwiOlwiREVcIixcImFkbWluXCI6ZmFsc2V9IiwidGVuYW50SWQiOiJERSIsImV4cCI6MTYxMjA1OTcwOCwiaWF0IjoxNjEyMTIxNzc0fQ.aYVRl-wXCCwU5aoeAIR92eKpG-e_egdq_5UnZhWU0jN1GdR2xqaMHOOzV6zCfC-Ic8hf7cofO2Zussy0htQX0Q',
      callbackUrl: null,
      trackingId: null,
      workitemId: null
    },
    resource: null,
    input: {
     "dataSetName": "ds:global:bolasnrecords",
     "source": "../sadmin/DE_GET/",
     "dest": "../sadmin/GET_REPOSITORY/",
     "filesFound": true,
      "fileObjList": [
        {
          "filename": 'asn_101619_010920.txt',
          "contentType": 'text/plain',
          "timestamp": '2020/02/02 22:46:20',
          "contentSize": 61635580,
          "fullPath": '../sadmin/DE_GET/asn_101619_010920.txt',
          "uploadedfilename": 'b881-39d_asn_101619_010920.txt',
          "fileId": 'defile:7745b97d-16d2-4e3c-a11e-5120a5725574'
        }
      ]  
    },
    config: {}
  };
  
  