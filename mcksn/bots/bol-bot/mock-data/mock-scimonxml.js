exports.mockRequest = {
    context: {
      gatewayUrl: 'https://dynpro.decisionengines.ai',
      accessToken: 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1YmVydXNlciIsImF1ZCI6ImFueSIsInVzZXJEZXRhaWwiOiJ7XCJ1c2VybmFtZVwiOlwidWJlcnVzZXJcIixcImZpcnN0bmFtZVwiOlwidWJlcnVzZXJcIixcImxhc3RuYW1lXCI6XCJ1YmVyVXNlck5hbWVcIixcImVtYWlsXCI6bnVsbCxcImF1dGhvcml0aWVzXCI6W3tcImF1dGhvcml0eVwiOlwiUk9MRV9VU0VSXCJ9LHtcImF1dGhvcml0eVwiOlwiNWNjMzYxZTZlNDhjMDEwMDA4NDQxOWYwXCJ9XSxcImVuYWJsZWRcIjp0cnVlLFwidGVuYW50SWRcIjpudWxsLFwic3lzdGVtVXNlclwiOmZhbHNlfSIsImV4cCI6MTU4ODgwMzM5NiwiaWF0IjoxNTg4ODA4MjM1fQ.6RpwOx9l7C4jPu2iBVQQh5EEMFqAk5aqQwQbIYudofDLxGtZe28LHQb5HGS3qffJcpjCu5KCZDhz5nqvtWR14A',
      callbackUrl: null,
      trackingId: null,
      workitemId: null
    },
    resource: null,
    input: {
      "scacResponse": {
        "hasExtractedScacCode": false,
        "scacMessage": "No SCAC Code Extracted",
        "scacCode": "MKSN"
      },
      "confidentMatches": [
        {
          "data": {
            "refDoc": "8106813862",
            "externalRef": "804382339",
            "billOfLading": "80690524485",
            "searchRefDoc": "8106813862",
            "searchExternalRef": "804382339",
            "searchBolNumber": "80690524485",
            "rowIdentifier": "80690524485-804382339"
          },
          "db_id": "5e09a75e2a489d000a6a60e9",
          "bolProb": {
            "hasExactMatch": true
          },
          "refDocProb": {
            "matchPrecent": 0.9,
            "subStringMatch": true
          },
          "externalRefProb": {
            "matchPrecent": 0.5294117647058824,
            "minimalMatch": true
          },
          "extractionObject": {
            "extractedBolNumber": "80690524485",
            "extractedExternalRef": "03473358043823395",
            "extractedRefDoc": "106813862"
          },
          "overAllScore": 2.4294117647058826,
          "percentMatch": 0.8098039215686276
        }
      ],
      "aiExtraction": {
        "BOLNumber": "03473358043823395",
        "PONumber": "Type",
        "RawOCR": [
          "AEE 22/9243\nCONTACT JAMES",
          "VENT, TD\n02241-5202 USA",
          "Sac 7",
          "Collect",
          "—-_—",
          "Page No",
          "213 na",
          "RD",
          "13.088",
          "15202,",
          "Dast 1/L Car",
          "20690524485",
          "C.0.D.",
          "«0",
          "™",
          "22",
          "SCD",
          "Typa",
          "80690524485",
          "80690524485",
          "Prepaid",
          "COMMERCE",
          "™",
          "13103461",
          "DELY",
          "Drivar:",
          "MCKESSON",
          "Ack No",
          "Ack No",
          "LEVEL",
          "202040000",
          "item,",
          "tel X12",
          "C/0/6",
          "WARD Ate",
          "SHMT",
          "Rate",
          "Rata",
          "Ae Ne",
          "Piecas",
          "Total Collact",
          "Waybill Number",
          "B/C",
          ") Car Cd",
          "5.2.4",
          "FULL",
          "D Car Rev",
          "MES",
          "MER",
          "Total Collect",
          "106",
          "CONTD .",
          "Waight",
          "D Car Cd",
          "[31]",
          "20.48",
          "34.",
          "Trl No",
          "889-5000 visit us at WWW.ODFL.COM",
          "CTN",
          "ODFL Rev",
          "Named",
          "0 Car Cd",
          "Due ODPL (UBD",
          "Ttl Pcs",
          "(336)",
          "Bast I/L Car",
          "CIR = ag",
          "050",
          "CHG |",
          "0500",
          "CHK",
          "Prepaid",
          "Prepaid",
          "SANS RY",
          "Typa",
          "Type",
          "— Sort",
          "415202,",
          "ON",
          "Company:",
          "Haybill Nuaber",
          "OLIVE",
          "TT: Ems -",
          "RB10681 3862",
          "Tariff",
          "Cons ignea Cd",
          "MA",
          "PO BOX",
          "MCKESSON",
          "f]",
          "this",
          "MN.",
          "12/03/19",
          "DEL",
          "MS",
          "oF",
          "TOLL-FREE",
          "Prod rd dr kd",
          "= Inside",
          "SPT,",
          "— Inside",
          "Purchasa Ord No",
          "Pieces",
          "0 Car Rev",
          "Shippar B/L Number",
          "py of",
          "Company:",
          "Company:",
          "S.3",
          "03473358043823395",
          "{1",
          "OREO Cos)",
          "20-00",
          "DOM",
          "— Gate",
          "Haybill Number",
          "lye",
          "23509",
          "RECEIVED IN GOOD CONDITION EXCEPT AS NOTED",
          "Dascription",
          "Dascription",
          "Dest Tre",
          "Data:",
          "a",
          "12.8.4:",
          "D/Agt",
          "D/Agt",
          "Bill to Cd",
          "SU",
          "Bill to Cd",
          "SI.",
          "Ttl AS Hgt",
          "Purchasa Ord No",
          "499",
          "Ttl Haight",
          "i388",
          "§",
          "|",
          "?",
          "«",
          "|",
          "=",
          "22",
          "|",
          "ll",
          "=",
          "2.8",
          "LATE",
          "Bill to Cd",
          "PO BOX",
          "SHHT",
          "CHK",
          "3».",
          "Data Del *d",
          "£05252",
          "Dascription",
          "NOT",
          "‘now",
          "vast 1/71. Car",
          "Articles",
          "Pcs. Del 'd",
          "Consignee Cd",
          "SHY",
          "385",
          "Date Del ‘d",
          "LJ Sag - Placas",
          "Orig Tra",
          "Weight",
          "IC.0.D.",
          "034: 7235504:3.3232¢5",
          "Tariff",
          "Tariff",
          "CORPORATION",
          "SPECIAL",
          "ER",
          "NITS:",
          "MA",
          "RES",
          "12/03/19",
          "-— Lift",
          "RY",
          "No",
          "no.",
          "NO",
          "R/C",
          "AFTER",
          "-752=3235",
          "8/C",
          "BRANCH",
          "BRANCH",
          "SHH",
          "Consignea Cd",
          "OLIVE",
          "Total Prepaid",
          "Total Prepaid",
          "Shipper",
          "no h",
          "w/",
          "A220.",
          "ON",
          "ON",
          "12.877",
          "1226)",
          "MCKESSON",
          "DOMINION FREIGHT LINE, INC. (OD\nPO BOX 415202, - BOSTON, NA 02741-5202 USA\n(336) BB9-5000 Visit us at WWW.ODFL.COM",
          "RANT",
          "W/B Date",
          "W/B Data",
          "4957237",
          "MANAGEMENT",
          "Date Del 'd",
          "PRIORITY",
          "2",
          "Dast Tre",
          "2",
          "Orig Tram",
          "\\¥ 2",
          "2)",
          "2",
          "2",
          "2)",
          "Total Prepaid",
          "— Dalivery",
          "Chemicals,",
          "Other",
          "COPY",
          "(11)",
          "10",
          "13 =",
          "B|",
          "Vial",
          "Due ODFLIU8D",
          "re..erence",
          "PROJECT",
          "=r",
          "DANG",
          "‘amt Yt wv Wm",
          "ANA",
          "2",
          "\" BOSTON,",
          "2/22/08",
          "Sat |",
          "-",
          "ATTY gn?",
          "gui gumic gum",
          "Naybill Number",
          "Shrink Wrap Intact Unless\nOtherwise Indicated",
          "2\n4",
          "Ack No",
          "ROLLOUT",
          "COPY",
          "COPY",
          "Copy",
          "Copy",
          "5",
          "Copy",
          "Shippar",
          "Indy",
          "- Seg Pincas",
          "MER",
          "VENT, ThE\n02241-5202 USA",
          "For ge = lw",
          "POLK",
          "839-5000 visit us at WHR.ODFL.COM",
          "PROJECT",
          "Other",
          "120419",
          "Ttl Weight",
          "OF",
          "Rate",
          "DC",
          "alt Well",
          "12",
          "~~",
          "|",
          "=",
          ")",
          "#",
          "[",
          "\\",
          "|",
          "P/C",
          "Medicines",
          "|",
          "\\",
          "gm gm",
          "ASH eHIohY",
          "ODFL Rav",
          "ODFL Rav",
          "Pcs. Dal \"dd",
          "Master Bill No",
          "\"J",
          "EF Shrink Wrap Intact Unless",
          "RONHGOS 24485",
          "Seal",
          "CONTD _",
          "0:1",
          "08812",
          "04",
          "(SATIN",
          "=,",
          "=",
          "\\",
          "|",
          "{",
          "———",
          "#",
          "|",
          "Deliver:",
          "Waybill Number",
          "~~",
          "<",
          "=",
          "[",
          "=",
          "Sac 7",
          "”",
          "=,",
          "&",
          "- Otherwise Indicated",
          "cop!",
          "C=",
          "—",
          "SE\ncod",
          "Dest Tram",
          "Ttl AS Hot",
          "S|",
          "BOSTON,",
          "=",
          "385",
          "MES",
          "Co.",
          "¢",
          "cl",
          "-245=23215",
          "AT ASS\nI Nay",
          "\\",
          "[2/04/06\nIon”",
          "shrink Wrap Intact Unlass\nOtherwise Indicated",
          "POLK",
          "— Residential",
          "= Residential",
          "CT.",
          "SRV. Ci-=01",
          "LANE",
          "LANE",
          "anc",
          "P/C",
          "120419",
          "SOW",
          "\\",
          "Drugs,",
          "1219 ..",
          "Lift",
          "DEMAND",
          "4",
          "4",
          "106",
          "106",
          "CASH",
          "value",
          "2106813862",
          "03473358043823395",
          "# (Af Apple)",
          "{VA",
          "PICKUP",
          "Haipht",
          "Lift",
          "UHL Kay",
          "Vr",
          "VS",
          "ERAS",
          "SURCHARGE",
          "D Car Rev",
          "JR.22",
          "Haybill Humber",
          "— Gate",
          "| ame We =",
          "$1",
          "1-0",
          "Seal",
          "Driver:",
          "O/Agt",
          "D Car Cd",
          "i",
          "wie she whe ale ale abe ale",
          "CORPORATION",
          "Master Bill No",
          "Master Bill No",
          "PHARMACEUTICAL",
          "PHARMACEUTICAL",
          "C/0/06",
          "211",
          "38654",
          "38654",
          "Data:",
          "Data:",
          "SPEED",
          "yr",
          "HAN",
          "~ Residential",
          "2.88",
          "4",
          "Ado:",
          "2.87",
          "SCHEDULED",
          "BRANCH",
          "DELIVERY RECEIPT",
          "LADS",
          "DELIVERY RECEIPT",
          "0 Car Cd",
          "0 Car Cd",
          "| 80690524485",
          "VIS",
          "Shipper B/L Number",
          "Ttl Pes",
          "Collect",
          "06:00",
          "485232",
          "OLD",
          "Page No",
          "Page No",
          "Cs:",
          "Trl No",
          "Trl No",
          "0 Car W/B No",
          "D/Agt",
          "Saal",
          "CASH",
          "CASH",
          "COMMERCE",
          "COMMERCE",
          "210681 3862",
          "060000",
          "be",
          "RD",
          "Ba:",
          "RD)",
          "Bl",
          "SU",
          "OU Car B/E No",
          "Drip Trm",
          "found .",
          "a",
          "Ttl AS Hgt",
          "R/C",
          "R/C",
          "HOLL IVE",
          "R/C",
          "IDI, INC",
          "FaYaV ad aVYaladisaW.N. Fad",
          "OD",
          "ceC..a",
          "Shipper",
          "\"OR",
          "0D Car B/B No",
          "ODFL Rav",
          "CHK",
          "_ Gate",
          "Tt1l Bee",
          "SOW",
          "{VA",
          "PHARMACEUTICAL,",
          "RECEIVED IN GOOD CONDITION EXCEPT AS NOTED",
          "RECEIVED IN GOOD CONDITION EXCEPT AS NOTED",
          "38654",
          "0 Car Rav",
          "CHG",
          "CHG |",
          "AS Weight",
          "0",
          "= Bert",
          "0",
          "0)",
          "‘0",
          "PD BOX",
          "Purchase Ord No",
          "— Delivery",
          "AS Height",
          "HM",
          "5",
          "ALES Max LING",
          "Be’ Bam Bal”",
          "07Agt",
          "—",
          "= Inside",
          "5",
          "08517",
          "UL)",
          "08512",
          "13462976",
          "Total Collect",
          "Pes. Dal *d",
          "— Delivery",
          "— Delivery",
          "— Delivery",
          "¥",
          "— Delivery",
          "DELIVERY",
          "# (if Applc)",
          "vi'S",
          "# (if Apple)",
          "No",
          "BTWN",
          "Ttl Haight",
          "Collact",
          "Other",
          "5",
          "D0 Car Kev",
          "1]",
          "oF SF Es =F",
          "D Car Rav",
          "DEF1 Ray",
          "Shipper B/L Number",
          "DELIVERY RECEIPT",
          "LANE",
          "Due ODPL UBD:",
          "1800",
          "—",
          "\\",
          "—",
          "|",
          "«©",
          "~",
          "3 |",
          "- Sort",
          "DOM",
          "TYPE",
          "CRANBURY"
        ],
        "TrailerNumber": "4957237",
        "currency": "CNY",
        "values_by_types": {
          "Amount": [
            {
              "Quantity": 13088,
              "Raw": "13.088"
            },
            {
              "Quantity": 15202,
              "Raw": "15202,"
            },
            {
              "Quantity": 20690524485,
              "Raw": "20690524485"
            },
            {
              "Quantity": 22,
              "Raw": "22"
            },
            {
              "Quantity": 80690524485,
              "Raw": "| 80690524485"
            },
            {
              "Quantity": 80690524485,
              "Raw": "80690524485"
            },
            {
              "Quantity": 13103461,
              "Raw": "13103461"
            },
            {
              "Quantity": 202040000,
              "Raw": "202040000"
            },
            {
              "Quantity": 106,
              "Raw": "106"
            },
            {
              "Quantity": 31,
              "Raw": "[31]"
            },
            {
              "Quantity": 20.48,
              "Raw": "20.48"
            },
            {
              "Quantity": 34,
              "Raw": "34."
            },
            {
              "Quantity": 336,
              "Raw": "(336)"
            },
            {
              "Quantity": 50,
              "Raw": "050"
            },
            {
              "Quantity": 500,
              "Raw": "0500"
            },
            {
              "Raw": "20-00"
            },
            {
              "Quantity": 23509,
              "Raw": "23509"
            },
            {
              "Quantity": 499,
              "Raw": "499"
            },
            {
              "Quantity": 22,
              "Raw": "22"
            },
            {
              "Quantity": 2.8,
              "Raw": "2.8"
            },
            {
              "Quantity": 5252,
              "Raw": "£05252"
            },
            {
              "Quantity": 385,
              "Raw": "385"
            },
            {
              "Quantity": -7523235,
              "Raw": "-752=3235"
            },
            {
              "Quantity": 12877,
              "Raw": "12.877"
            },
            {
              "Quantity": 1226,
              "Raw": "1226)"
            },
            {
              "Quantity": 4957237,
              "Raw": "4957237"
            },
            {
              "Quantity": 11,
              "Raw": "(11)"
            },
            {
              "Quantity": 10,
              "Raw": "10"
            },
            {
              "Quantity": 13,
              "Raw": "13 ="
            },
            {
              "Quantity": 24,
              "Raw": "2\n4"
            },
            {
              "Quantity": 120419,
              "Raw": "120419"
            },
            {
              "Quantity": 12,
              "Raw": "12"
            },
            {
              "CurrencyCode": "RON",
              "Quantity": 24485,
              "Raw": "RONHGOS 24485",
              "Symbol": "lei"
            },
            {
              "Quantity": 8812,
              "Raw": "08812"
            },
            {
              "Quantity": 4,
              "Raw": "04"
            },
            {
              "Quantity": 385,
              "Raw": "385"
            },
            {
              "Quantity": -24523215,
              "Raw": "-245=23215"
            },
            {
              "Quantity": 120419,
              "Raw": "120419"
            },
            {
              "Quantity": 1219,
              "Raw": "1219 .."
            },
            {
              "Quantity": 106,
              "Raw": "106"
            },
            {
              "Quantity": 106,
              "Raw": "106"
            },
            {
              "Quantity": 2106813862,
              "Raw": "2106813862"
            },
            {
              "Raw": "1-0"
            },
            {
              "Quantity": 211,
              "Raw": "211"
            },
            {
              "Quantity": 38654,
              "Raw": "38654"
            },
            {
              "Quantity": 38654,
              "Raw": "38654"
            },
            {
              "Quantity": 2.88,
              "Raw": "2.88"
            },
            {
              "Quantity": 2.87,
              "Raw": "2.87"
            },
            {
              "Quantity": 80690524485,
              "Raw": "80690524485"
            },
            {
              "Quantity": 2106813862,
              "Raw": "210681 3862"
            },
            {
              "Quantity": 60000,
              "Raw": "060000"
            },
            {
              "Quantity": 415202,
              "Raw": "415202,"
            },
            {
              "Quantity": 485232,
              "Raw": "485232"
            },
            {
              "Quantity": 38654,
              "Raw": "38654"
            },
            {
              "Quantity": 8517,
              "Raw": "08517"
            },
            {
              "Quantity": 8512,
              "Raw": "08512"
            },
            {
              "Quantity": 13462976,
              "Raw": "13462976"
            },
            {
              "Quantity": 1800,
              "Raw": "1800"
            }
          ],
          "Date": [
            {
              "Day": 5,
              "Month": 1,
              "Raw": "15202,",
              "Year": 2020
            },
            {
              "Day": 3,
              "Month": 12,
              "Raw": "12/03/19",
              "Year": 2019
            },
            {
              "Day": 4,
              "Month": 8,
              "Raw": "12.8.4:",
              "Year": 2012
            },
            {
              "Day": 3,
              "Month": 12,
              "Raw": "12/03/19",
              "Year": 2019
            },
            {
              "Day": 22,
              "Month": 2,
              "Raw": "2/22/08",
              "Year": 2008
            },
            {
              "Day": 2,
              "Month": 3,
              "Raw": "-245=23215",
              "Year": 2452
            },
            {
              "Day": 2,
              "Month": 1,
              "Raw": "[2/04/06\nIon”",
              "Year": 2006
            }
          ],
          "Identifier": [
            {
              "Raw": "213 na",
              "Value": "213 na"
            },
            {
              "Raw": "13.088",
              "Value": "13.088"
            },
            {
              "Raw": "20690524485",
              "Value": "20690524485"
            },
            {
              "Raw": "| 80690524485",
              "Value": "| 80690524485"
            },
            {
              "Raw": "80690524485",
              "Value": "80690524485"
            },
            {
              "Raw": "13103461",
              "Value": "13103461"
            },
            {
              "Raw": "202040000",
              "Value": "202040000"
            },
            {
              "Raw": "C/0/6",
              "Value": "C/0/6"
            },
            {
              "Raw": "5.2.4",
              "Value": "5.2.4"
            },
            {
              "Raw": "106",
              "Value": "106"
            },
            {
              "Raw": "[31]",
              "Value": "[31]"
            },
            {
              "Raw": "20.48",
              "Value": "20.48"
            },
            {
              "Raw": "(336)",
              "Value": "(336)"
            },
            {
              "Raw": "050",
              "Value": "050"
            },
            {
              "Raw": "0500",
              "Value": "0500"
            },
            {
              "Raw": "RB10681 3862",
              "Value": "RB10681 3862"
            },
            {
              "Raw": "03473358043823395",
              "Value": "03473358043823395"
            },
            {
              "Raw": "20-00",
              "Value": "20-00"
            },
            {
              "Raw": "23509",
              "Value": "23509"
            },
            {
              "Raw": "499",
              "Value": "499"
            },
            {
              "Raw": "i388",
              "Value": "i388"
            },
            {
              "Raw": "2.8",
              "Value": "2.8"
            },
            {
              "Raw": "£05252",
              "Value": "£05252"
            },
            {
              "Raw": "385",
              "Value": "385"
            },
            {
              "Raw": "034: 7235504:3.3232¢5",
              "Value": "034: 7235504:3.3232¢5"
            },
            {
              "Raw": "-752=3235",
              "Value": "-752=3235"
            },
            {
              "Raw": "A220.",
              "Value": "A220."
            },
            {
              "Raw": "12.877",
              "Value": "12.877"
            },
            {
              "Raw": "1226)",
              "Value": "1226)"
            },
            {
              "Raw": "4957237",
              "Value": "4957237"
            },
            {
              "Raw": "(11)",
              "Value": "(11)"
            },
            {
              "Raw": "13 =",
              "Value": "13 ="
            },
            {
              "Raw": "2\n4",
              "Value": "2\n4"
            },
            {
              "Raw": "120419",
              "Value": "120419"
            },
            {
              "Raw": "RONHGOS 24485",
              "Value": "RONHGOS 24485"
            },
            {
              "Raw": "0:1",
              "Value": "0:1"
            },
            {
              "Raw": "08812",
              "Value": "08812"
            },
            {
              "Raw": "385",
              "Value": "385"
            },
            {
              "Raw": "120419",
              "Value": "120419"
            },
            {
              "Raw": "1219 ..",
              "Value": "1219 .."
            },
            {
              "Raw": "106",
              "Value": "106"
            },
            {
              "Raw": "106",
              "Value": "106"
            },
            {
              "Raw": "2106813862",
              "Value": "2106813862"
            },
            {
              "Raw": "03473358043823395",
              "Value": "03473358043823395"
            },
            {
              "Raw": "JR.22",
              "Value": "JR.22"
            },
            {
              "Raw": "1-0",
              "Value": "1-0"
            },
            {
              "Raw": "C/0/06",
              "Value": "C/0/06"
            },
            {
              "Raw": "211",
              "Value": "211"
            },
            {
              "Raw": "38654",
              "Value": "38654"
            },
            {
              "Raw": "38654",
              "Value": "38654"
            },
            {
              "Raw": "2.88",
              "Value": "2.88"
            },
            {
              "Raw": "2.87",
              "Value": "2.87"
            },
            {
              "Raw": "80690524485",
              "Value": "80690524485"
            },
            {
              "Raw": "06:00",
              "Value": "06:00"
            },
            {
              "Raw": "210681 3862",
              "Value": "210681 3862"
            },
            {
              "Raw": "060000",
              "Value": "060000"
            },
            {
              "Raw": "415202,",
              "Value": "415202,"
            },
            {
              "Raw": "485232",
              "Value": "485232"
            },
            {
              "Raw": "38654",
              "Value": "38654"
            },
            {
              "Raw": "07Agt",
              "Value": "07Agt"
            },
            {
              "Raw": "08517",
              "Value": "08517"
            },
            {
              "Raw": "08512",
              "Value": "08512"
            },
            {
              "Raw": "13462976",
              "Value": "13462976"
            },
            {
              "Raw": "1800",
              "Value": "1800"
            }
          ],
          "Number": [
            {
              "Raw": "13.088",
              "Value": "13.088"
            },
            {
              "Raw": "15202,",
              "Value": "15202,"
            },
            {
              "Raw": "20690524485",
              "Value": "20690524485"
            },
            {
              "Raw": "22",
              "Value": "22"
            },
            {
              "Raw": "| 80690524485",
              "Value": "| 80690524485"
            },
            {
              "Raw": "80690524485",
              "Value": "80690524485"
            },
            {
              "Raw": "13103461",
              "Value": "13103461"
            },
            {
              "Raw": "202040000",
              "Value": "202040000"
            },
            {
              "Raw": "5.2.4",
              "Value": "5.2.4"
            },
            {
              "Raw": "106",
              "Value": "106"
            },
            {
              "Raw": "[31]",
              "Value": "[31]"
            },
            {
              "Raw": "20.48",
              "Value": "20.48"
            },
            {
              "Raw": "34.",
              "Value": "34."
            },
            {
              "Raw": "(336)",
              "Value": "(336)"
            },
            {
              "Raw": "050",
              "Value": "050"
            },
            {
              "Raw": "0500",
              "Value": "0500"
            },
            {
              "Raw": "03473358043823395",
              "Value": "03473358043823395"
            },
            {
              "Raw": "20-00",
              "Value": "20-00"
            },
            {
              "Raw": "23509",
              "Value": "23509"
            },
            {
              "Raw": "12.8.4:",
              "Value": "12.8.4:"
            },
            {
              "Raw": "499",
              "Value": "499"
            },
            {
              "Raw": "22",
              "Value": "22"
            },
            {
              "Raw": "2.8",
              "Value": "2.8"
            },
            {
              "Raw": "385",
              "Value": "385"
            },
            {
              "Raw": "12.877",
              "Value": "12.877"
            },
            {
              "Raw": "1226)",
              "Value": "1226)"
            },
            {
              "Raw": "4957237",
              "Value": "4957237"
            },
            {
              "Raw": "2",
              "Value": "2"
            },
            {
              "Raw": "2",
              "Value": "2"
            },
            {
              "Raw": "2)",
              "Value": "2)"
            },
            {
              "Raw": "2",
              "Value": "2"
            },
            {
              "Raw": "2",
              "Value": "2"
            },
            {
              "Raw": "2",
              "Value": "2"
            },
            {
              "Raw": "(11)",
              "Value": "(11)"
            },
            {
              "Raw": "10",
              "Value": "10"
            },
            {
              "Raw": "2)",
              "Value": "2)"
            },
            {
              "Raw": "5",
              "Value": "5"
            },
            {
              "Raw": "120419",
              "Value": "120419"
            },
            {
              "Raw": "12",
              "Value": "12"
            },
            {
              "Raw": "0:1",
              "Value": "0:1"
            },
            {
              "Raw": "08812",
              "Value": "08812"
            },
            {
              "Raw": "04",
              "Value": "04"
            },
            {
              "Raw": "385",
              "Value": "385"
            },
            {
              "Raw": "120419",
              "Value": "120419"
            },
            {
              "Raw": "1219 ..",
              "Value": "1219 .."
            },
            {
              "Raw": "4",
              "Value": "4"
            },
            {
              "Raw": "4",
              "Value": "4"
            },
            {
              "Raw": "106",
              "Value": "106"
            },
            {
              "Raw": "106",
              "Value": "106"
            },
            {
              "Raw": "2106813862",
              "Value": "2106813862"
            },
            {
              "Raw": "03473358043823395",
              "Value": "03473358043823395"
            },
            {
              "Raw": "1-0",
              "Value": "1-0"
            },
            {
              "Raw": "211",
              "Value": "211"
            },
            {
              "Raw": "38654",
              "Value": "38654"
            },
            {
              "Raw": "38654",
              "Value": "38654"
            },
            {
              "Raw": "2.88",
              "Value": "2.88"
            },
            {
              "Raw": "4",
              "Value": "4"
            },
            {
              "Raw": "2.87",
              "Value": "2.87"
            },
            {
              "Raw": "80690524485",
              "Value": "80690524485"
            },
            {
              "Raw": "06:00",
              "Value": "06:00"
            },
            {
              "Raw": "5",
              "Value": "5"
            },
            {
              "Raw": "060000",
              "Value": "060000"
            },
            {
              "Raw": "415202,",
              "Value": "415202,"
            },
            {
              "Raw": "485232",
              "Value": "485232"
            },
            {
              "Raw": "38654",
              "Value": "38654"
            },
            {
              "Raw": "0",
              "Value": "0"
            },
            {
              "Raw": "0",
              "Value": "0"
            },
            {
              "Raw": "0)",
              "Value": "0)"
            },
            {
              "Raw": "5",
              "Value": "5"
            },
            {
              "Raw": "08517",
              "Value": "08517"
            },
            {
              "Raw": "08512",
              "Value": "08512"
            },
            {
              "Raw": "13462976",
              "Value": "13462976"
            },
            {
              "Raw": "5",
              "Value": "5"
            },
            {
              "Raw": "1]",
              "Value": "1]"
            },
            {
              "Raw": "1800",
              "Value": "1800"
            },
            {
              "Raw": "3 |",
              "Value": "3 |"
            }
          ],
          "address": [
            {
              "AddressLine1": "060000 re. erence \\¥ 2",
              "CountryCode": "no",
              "Locality": "satin",
              "bbox": [
                267,
                852,
                393,
                139
              ],
              "text": "re..erence \\¥ 2 060000 12.8.4: 120419 (SATIN NO"
            },
            {
              "AddressLine1": "13",
              "Code": "15202",
              "Company": "\\ on =r a 4",
              "Locality": "=",
              "Phone": "(336) 889-5000",
              "bbox": [
                365,
                1717,
                893,
                95
              ],
              "text": "\\ ON =r a 4 13 = Vr 15202, BOSTON, MA VENT, ThE\n02241-5202 USA (336) 889-5000 visit us at WWW.ODFL.COM"
            },
            {
              "AddressLine1": "13 =r vr 4",
              "Locality": "boston",
              "bbox": [
                577,
                1717,
                236,
                67
              ],
              "text": "=r Vr 4 13 = BOSTON,"
            }
          ]
        }
      }
    },
    config: {}
  };
  
  