/** utility package objects available to be included as needed:
* lodash https://lodash.com/docs/
* aws-sdk: https://www.npmjs.com/package/aws-sdk
* json2csv: https://www.npmjs.com/package/json2csv
* nunjucks: https://www.npmjs.com/package/nunjucks
* request: https://www.npmjs.com/package/request
* request-promise: https://www.npmjs.com/package/request-promise
* yauzl-promise https://www.npmjs.com/package/yauzl-promise
* ssh2: https://www.npmjs.com/package/ssh2
* stream-buffers: https://www.npmjs.com/package/stream-buffers

* include using require pattern, i.e.
* const _ = require('lodash');
*/


let input = request.input;
let config = request.config;
let context = request.context;
let resource = request.resource;
let gatewayUrl = context.gatewayUrl;
let token = context.accessToken;
let callbackUrl = context.callbackUrl;
let tracking_id = context.trackingId;
let workitem_id = context.workitemId;

const js2xml = require('js2xmlparser');
const {parseString} = require('xml2js');
// const fs = require('fs');
const rp = require('request-promise');
const hasError = false;
const failProcess = false;
const BillofLading = {};
let ContextID=undefined;
const AIBotOutput = input.aiExtraction;
const confidentMatchObj = input.confidentMatches;
const TrailerNumberDefault = input.trailerNumberDefault;
const DriverNameDefault = input.driverNameDefault;
// const SCACDefault = input.ScacResponse.scacCode;

const TrailerNumber = AIBotOutput.TrailerNumber || TrailerNumberDefault;
const DriverName = AIBotOutput.DriverName || DriverNameDefault;
const SCAC = input.scacResponse.scacCode;

// Iterate through BillofLading Object to get BOL Nos
const BOLNos = [];
confidentMatchObj.forEach(function(obj) {
  BOLNos.push(obj.data.billOfLading);
});
// console.log(BOLNos);

// Error Handler
const handleErrors = (hasError, failProcess, errorDescription) => {
  hasError = hasError;
  failProcess = failProcess;
  console.log('BOT ERROR===>' + errorDescription);
};

var now = new Date();

//Get Today's date in format yyyymmdd
const dateToday = () => {
    var y = now.getFullYear();
    var m = now.getMonth() + 1;
    var d = now.getDate();
    return '' + y + (m < 10 ? '0' : '') + m + (d < 10 ? '0' : '') + d;
};
const ArrivalDate = dateToday();

//Get Current Time in format hh:mm:ss
const timeNow = () => {
    var hour = now.getHours();
    var minute = now.getMinutes();
    var second = now.getSeconds();
    return (("0" + hour).slice(-2) + ":" + ("0" + minute).slice(-2) + ":" + ("0" + second).slice(-2));
};
const ArrivalTime = timeNow();

// McKesson Environment details
const getEnvironmentDetails = () => {
  return {
    Mck: {
      baseUri: 'http://1234.12.1.2/',
    },
    Scimon: {
      baseUri: 'https://dynmis.dynpro.com/',
      invoiceEndPoint: 'test/xmlTest.php',
      invoiceEndPoint1: 'test/xmlRes.php',
    },
  };
};

const createRequestObj = (action, dataToProcess) => {
  try {
    const requestObj = {};
    const envDetail = getEnvironmentDetails();
    let requestUri;
    let body = {};

    switch (action) {
      case 'ScimonLoginRequest':
        requestUri = envDetail.Scimon.baseUri + envDetail.Scimon.invoiceEndPoint;
        requestObj.headers = {
          'Content-Type': 'application/xml',
        };
        requestObj.method = 'POST';
        requestObj.uri = requestUri;
        body = dataToProcess;
        requestObj.body = body;
        break;
      case 'ScimonBOLRequest':
        requestUri = envDetail.Scimon.baseUri + envDetail.Scimon.invoiceEndPoint1;
        requestObj.headers = {
          'Content-Type': 'application/xml',
        };
        requestObj.method = 'POST';
        requestObj.uri = requestUri;
        body = dataToProcess;
        requestObj.body = body;
        break;

      default:
        break;
    }

    return requestObj;
  } catch (err) {
    handleErrors(true, true, 'generateDeBotRequestObj' + err);
  }
};

// Login XML Schema
const loginxmlSchema = {
  'LoginRequest': {
    'UserName': 'test',
    'Password': 'test',
    'Environment': 'test',
    'Warehouse': 'test',
    'Company': 'test',
    'Division': 'test',
    'CallCustom': 'test',
    'UserDefined1': 'test',
    'UserDefined2': 'test',
    'UserDefined3': 'test',
  },
};

// final XML Schema with Rsponse Data.
const generateBOLschema =()=>{
  const schema={
    'GuardCheckinRequest': {
      'ContextID': ContextID,
      'TrailerNumber': TrailerNumber,
      'DriverName': DriverName,
      'SCAC': SCAC,
      'ArrivalDate': ArrivalDate,
      'ArrivalTime': ArrivalTime,
      'BillofLading': {
        'BOL': BOLNos,
      },
      'UserDefined1': 'test',
      'UserDefined2': 'test',
      'UserDefined3': 'test',
    },
  };
  return schema;
};
const parseResponse = (responseXml, toggle)=>{
  let responseJson = undefined;
  parseString(responseXml, {trim: true, explicitArray: false}, function(err, result) {
    responseJson= result;
  });
  switch (toggle) {
    case 'ScimanLogin':
      return responseJson.LoginResponse;
    case 'ScimanBOL':
      return responseJson;
  }
};
const convertToXml=(schema)=>{
  const convertedXml = js2xml.parse('xml', schema, {declaration: {encoding: 'UTF-8', include: true}});
  return convertedXml;
};
const mainBotExecution = async () => {
  try {
    const xmlLoginString = convertToXml(loginxmlSchema);
    const loginRequestObject = createRequestObj('ScimonLoginRequest', xmlLoginString);
    const loginRequestResponse = await rp(loginRequestObject);
    const loginResponseObj=parseResponse(loginRequestResponse, 'ScimanLogin');
    ContextID = loginResponseObj.ContextID;
    const BOLJsonSchema=generateBOLschema();
    const xmlBOLSchema = convertToXml(BOLJsonSchema);
    const BOLPostObj = createRequestObj('ScimonBOLRequest', xmlBOLSchema);
    const BOLPostResponse = await rp(BOLPostObj);
    const ScimanResponseObj = parseResponse(BOLPostResponse, 'ScimanBOL');
    console.log(ScimanResponseObj);
    // fs.writeFileSync('./myobj.json', JSON.stringify(ScimanResponseObj, null, 2));
    // return ScimanResponseObj;
    response.data = ScimanResponseObj;
    return response;
  } catch (err) {
    handleErrors(true, true, 'mainBotExecution' + err);
  }
};

return mainBotExecution();