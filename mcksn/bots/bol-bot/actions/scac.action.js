/** utility package objects available to be included as needed:
* lodash https://lodash.com/docs/
* aws-sdk: https://www.npmjs.com/package/aws-sdk
* json2csv: https://www.npmjs.com/package/json2csv
* nunjucks: https://www.npmjs.com/package/nunjucks
* request: https://www.npmjs.com/package/request
* request-promise: https://www.npmjs.com/package/request-promise
* yauzl-promise https://www.npmjs.com/package/yauzl-promise
* ssh2: https://www.npmjs.com/package/ssh2
* stream-buffers: https://www.npmjs.com/package/stream-buffers

* include using require pattern, i.e.
* const _ = require('lodash');
*/

let input = request.input;
let config = request.config;
let context = request.context;
let resource = request.resource;
let gatewayUrl = context.gatewayUrl;
let token = context.accessToken;
let callbackUrl = context.callbackUrl;
let tracking_id = context.trackingId;
let workitem_id = context.workitemId;

const dataSetName = input.dataSetName;

//Use below code for local testing
// const token = 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJTeXN0ZW1Vc2VyIiwiYXVkIjoiYW55IiwidXNlckRldGFpbCI6IntcInVzZXJuYW1lXCI6XCJTeXN0ZW1Vc2VyXCIsXCJmaXJzdG5hbWVcIjpudWxsLFwibGFzdG5hbWVcIjpudWxsLFwiZW1haWxcIjpudWxsLFwiYXV0aG9yaXRpZXNcIjpbe1wiYXV0aG9yaXR5XCI6XCJST0xFX1NZU1RFTVwifV0sXCJlbmFibGVkXCI6dHJ1ZSxcInN5c3RlbVVzZXJcIjp0cnVlfSIsImlhdCI6MTUxMjM4NTc3NH0.pG9i3Y5baS_i52MmYwdhQJtrrOyZWqqnwzfA8rRgU7QQyjjNCEgEqU1ses2iSQ-ZplYW3BLkUzYobO6vHunu4w';
// const gatewayUrl = 'https://dynpro.decisionengines.ai';
const rp = require('request-promise');
const _ = require('lodash');
const handleErrors = (hasError, failProcess, errorDescription) => {
  hasError = hasError;
  failProcess = failProcess;

  if (failProcess == true) {
    console.log('BOT ERROR======================== ' + errorDescription);
    return {
      statusCode: 'FAILED',
      errorMessage: errorDescription,
      data: {},
    };
  }
};
const defaultSCAC = input.scacDefault;
const makeApiRequests = async (data = {}, endpointUri, method) => {
  try {
    const options = {
      method: method,
      uri: `${gatewayUrl}/${endpointUri}`,
      headers: {
        'Authorization': 'Bearer ' + token,
      },
      body: data,
      rejectUnauthorized: false,
      json: true,
    };

    const responseObj = await rp(options);
    return responseObj;
  } catch (err) {
    handleErrors(true, true, 'BOT ERROR makeApiRequests =====================> ' + err);
  }
};
const searchDataSetUsingQuery = async (query, type) => {
  try {
    const requestObject = query;
    // const searchResponse = await makeApiRequests(requestObject, `datasets/type/standard/instance/${dataSetName}/search`, 'POST');
    const searchResponse = await makeApiRequests(requestObject, `datasets/dataset/${dataSetName}/record/search`, 'POST');
    if (searchResponse) {
      return searchResponse;
    } else {
      return {
        'content': [],
      };
    }
  } catch (err) {
    handleErrors(true, true, 'BOT ERROR getCombinedIdForAsnId =====================> ' + err);
  }
};
const createResponse = (extractedSCAC, defaultSCAC, ScacFound)=>{
  try {
    let responseObj = {};
    switch (ScacFound) {
      case 'SCAC not found':
        responseObj = {
          hasExtractedScacCode: false,
          scacMessage: 'No SCAC Code Extracted',
          scacCode: defaultSCAC,
        };
        break;
      case 'SCAC match found':
        responseObj = {
          hasExtractedScacCode: true,
          hasScacMatch: true,
          scacMessage: 'SCAC Code Found '+extractedSCAC,
          scacCode: extractedSCAC,
        };
        break;
      case 'SCAC match not found':
        responseObj = {
          hasExtractedScacCode: true,
          hasScacMatch: false,
          scacMessage: 'Extracted SCAC code does not match '+extractedSCAC+'. Using Default',
          scacCode: defaultSCAC,
        };
        break;
      default:
        responseObj={name: 'success'};
    }
    return responseObj;
  } catch (err) {
    handleErrors(true, true, 'BOT ERROR createResponse =====================> ' + err);
  }
};
const maninBotExecution = async ()=>{
  try {
    // let response = {};
    let matchFlag = false;

    if (!(input.extractedData.hasOwnProperty('scacCode'))) {
      response.data.scacResponse = createResponse('', defaultSCAC, 'SCAC not found');
      return response;
    }
    const extractedSCAC = _.get(input, 'extractedData.scacCode', String);

    const searchCriteria = [{
      "fields": {
        "data": "true",
      },
      "filter": {
        "data.scacCode": {
          "$in": [extractedSCAC],
        },
      },
      "sort": {},
    },
    ];

    const searchResults = await Promise.all(searchCriteria.map(async (searchParam) => {
      const result = await searchDataSetUsingQuery(searchParam, 'standard');
      result.query = searchParam;
      return result;
    }));
   
    const searchedSCACs = searchResults[0].content;
    // console.log(searchedSCACs.length)
    if (searchedSCACs.length>0) {
      const datasetSCAC = searchedSCACs[0].data.scacCode;
      if (datasetSCAC == extractedSCAC) {
        matchFlag=true;
      }
    }
    if (matchFlag==true) {
      response.data.scacResponse = createResponse(extractedSCAC, defaultSCAC, 'SCAC match found');
    } else {
      response.data.scacResponse = createResponse(extractedSCAC, defaultSCAC, 'SCAC match not found');
    }
    console.log(response);
    return response;
  } catch (err) {
    handleErrors(true, true, 'BOT ERROR maninBotExecution =====================> ' + err);
  }
};
return maninBotExecution();
