/** utility package objects available to be included as needed:
* lodash https://lodash.com/docs/
* aws-sdk: https://www.npmjs.com/package/aws-sdk
* json2csv: https://www.npmjs.com/package/json2csv
* nunjucks: https://www.npmjs.com/package/nunjucks
* request: https://www.npmjs.com/package/request
* request-promise: https://www.npmjs.com/package/request-promise
* yauzl-promise https://www.npmjs.com/package/yauzl-promise
* ssh2: https://www.npmjs.com/package/ssh2
* stream-buffers: https://www.npmjs.com/package/stream-buffers

* include using require pattern, i.e.
* const _ = require('lodash');
*/

let input = request.input;
let config = request.config;
let context = request.context;
let resource = request.resource;
let gatewayUrl = context.gatewayUrl;
let token = context.accessToken;
let callbackUrl = context.callbackUrl;
let tracking_id = context.trackingId;
let workitem_id = context.workitemId;

// For local testing copy from here
// const token = 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJTeXN0ZW1Vc2VyIiwiYXVkIjoiYW55IiwidXNlckRldGFpbCI6IntcInVzZXJuYW1lXCI6XCJTeXN0ZW1Vc2VyXCIsXCJmaXJzdG5hbWVcIjpudWxsLFwibGFzdG5hbWVcIjpudWxsLFwiZW1haWxcIjpudWxsLFwiYXV0aG9yaXRpZXNcIjpbe1wiYXV0aG9yaXR5XCI6XCJST0xFX1NZU1RFTVwifV0sXCJlbmFibGVkXCI6dHJ1ZSxcInN5c3RlbVVzZXJcIjp0cnVlfSIsImlhdCI6MTUxMjM4NTc3NH0.pG9i3Y5baS_i52MmYwdhQJtrrOyZWqqnwzfA8rRgU7QQyjjNCEgEqU1ses2iSQ-ZplYW3BLkUzYobO6vHunu4w';
// const gatewayUrl = 'https://dynpro.decisionengines.ai';
const rp = require('request-promise');
const _ = require('lodash');
const fs = require('fs');
const dataSetName = input.dataSetName;
const possibleMatchLimit = input.possibleMatchLimit; // Configuration for restricting the number of possible matches to show
const BolThresHoldLowest = input.bolThresHoldLowest;
const BolThresHoldHighest = input.bolThresHoldHighest;
const RefThresHold = input.refThresHold;
const ExtRefThresHold = input.extRefThresHold;
const handleErrors = (hasError, failProcess, errorDescription) => {
  hasError = hasError;
  failProcess = failProcess;

  if (failProcess == true) {
    console.log('BOT ERROR======================== ' + errorDescription);
    return {
      statusCode: 'FAILED',
      errorMessage: errorDescription,
      data: {},
    };
  }
};
const searchDataSetUsingQuery = async (query, type) => {
  try {
    const requestObject = query;
    const searchResponse = await makeApiRequests(`datasets/dataset/${dataSetName}/record/search`, requestObject,'POST');
    if (searchResponse) {
      return searchResponse;
    } else {
      return {
        'content': [],
      };
    }
  } catch (err) {
    handleErrors(true, true, 'BOT ERROR getCombinedIdForAsnId =====================> ' + err);
  }
};
const makeApiRequests = async (endpointUri, data = {}, method) => {
  try {
    const options = {
      method: method,
      uri: `${gatewayUrl}/${endpointUri}`,
      headers: {
        'Authorization': 'Bearer ' + token,
      },
      body: data,
      rejectUnauthorized: false,
      json: true,
    };

    const responseObj = await rp(options);
    return responseObj;
  } catch (err) {
    handleErrors(true, true, 'BOT ERROR makeApiRequests =====================> ' + err);
  }
};
let arrayOfSearchFields = [];
const generateSearchParams = (fieldsToSearch, stringFieldsArray, filteredOCR) => {
  try {
    for (field of fieldsToSearch) {
      const currentField = field.Value;
      if (currentField.length >= 4) {
        arrayOfSearchFields.push(currentField);
        if (currentField.includes('-' || '/')) {
          const splittedString = currentField.split('-');

          splittedString.map(function(item) {
            // console.log("nonstripped" + item);
            const itemStripped = item.replace(/\W/g, '');
            const revStrip = itemStripped.replace(/[^\d]/g, '');
            if (revStrip.length >= 4) {
              arrayOfSearchFields.push(revStrip);
              console.log(revStrip + 'NOSPACE');
            }
          });
        }
        const stripedField = currentField.replace(/\W/g, '');
        if (stripedField != currentField && stripedField.length >= 4) {
          arrayOfSearchFields.push(stripedField);
        }
        const numbericOnly = stripedField.replace(/[^\d]/g, '');
        if (numbericOnly != stripedField && numbericOnly != currentField && numbericOnly != '' && numbericOnly.length >= 4) {
          arrayOfSearchFields.push(numbericOnly);
        }
        const removePrefix = numbericOnly.replace(/402|/, '');
        if (removePrefix != currentField && removePrefix != numbericOnly && removePrefix.length >= 4) {
          arrayOfSearchFields.push(removePrefix);
        }
        const removeLeadingZeroesAfterPrefix = removePrefix.replace(/^0+/, '');
        if (removeLeadingZeroesAfterPrefix != currentField && removeLeadingZeroesAfterPrefix != removePrefix && removeLeadingZeroesAfterPrefix.length >= 4) {
          arrayOfSearchFields.push(removeLeadingZeroesAfterPrefix);
        }

        const removedLeadingzeroes = currentField.replace(/^0+/, '');
        if (removedLeadingzeroes != currentField && removedLeadingzeroes != removeLeadingZeroesAfterPrefix && removedLeadingzeroes.length >= 4) {
          arrayOfSearchFields.push(removedLeadingzeroes);
        }

        const removedCompanySpecificPrefix = currentField.replace(/^[a-zA-Z]+/, '');
        if (removedCompanySpecificPrefix != currentField && removedCompanySpecificPrefix.length >= 4) {
          arrayOfSearchFields.push(removedCompanySpecificPrefix);
        }
      }
    }
    for (field of filteredOCR) {
      if (field.includes('-' || '/')) {
        const splittedString = field.split('-');

        splittedString.map(function(item) {
          // console.log("nonstripped" + item);
          const itemStripped = item.replace(/\W/g, '');
          const revStrip = itemStripped.replace(/[^\d]/g, '');
          if (revStrip.length >= 4) {
            arrayOfSearchFields.push(revStrip);
            console.log(revStrip + 'NOSPACE');
          }
        });
      }
      const stripedOCRField = field.replace(/\W/g, '');
      const OCRremovedLeadingzeroes = stripedOCRField.replace(/^0+/, '');

      const removedOCRCompanySpecificPrefix = OCRremovedLeadingzeroes.replace(/^[a-zA-Z]+/, '');
      if (removedOCRCompanySpecificPrefix != '' && removedOCRCompanySpecificPrefix.length >= 4) {
        arrayOfSearchFields.push(removedOCRCompanySpecificPrefix);
      }
      const OCRnumbericOnly = removedOCRCompanySpecificPrefix.replace(/[^\d]/g, '');
      if (OCRnumbericOnly != removedOCRCompanySpecificPrefix && OCRnumbericOnly != stripedOCRField && OCRnumbericOnly != '' && OCRnumbericOnly.length >= 4) {
        arrayOfSearchFields.push(OCRnumbericOnly);
      }
      const OCRremovePrefix = OCRnumbericOnly.replace(/402|/, '');
      if (OCRremovePrefix != removedOCRCompanySpecificPrefix && OCRremovePrefix != OCRnumbericOnly && OCRremovePrefix.length >= 4) {
        arrayOfSearchFields.push(OCRremovePrefix);
      }
      const OCRremoveLeadingZeroesAfterPrefix = OCRremovePrefix.replace(/^0+/, '');
      if (OCRremoveLeadingZeroesAfterPrefix != OCRremovePrefix && OCRremoveLeadingZeroesAfterPrefix != OCRnumbericOnly && OCRremoveLeadingZeroesAfterPrefix != '' && OCRremoveLeadingZeroesAfterPrefix.length >= 4) {
        arrayOfSearchFields.push(OCRremoveLeadingZeroesAfterPrefix);
      }
    }
    arrayOfSearchFields = _.uniq(arrayOfSearchFields);
    fs.writeFileSync('./SearchCriteria.txt', arrayOfSearchFields);
    console.log('TOTAL ARRAY AFTER UNIQ  ' + arrayOfSearchFields);
    arrayOfSearchFields = arrayOfSearchFields.concat(stringFieldsArray);

    const searchCriteria = [{
      'fields': {
        'data': 'true',
      },
      'filter': {
        'data.searchBolNumber': {
          '$in': arrayOfSearchFields,
        },
      },
      'sort': {},
    },
    {
      'fields': {
        'data': 'true',
      },
      'filter': {
        'data.searchExternalRef': {
          '$in': arrayOfSearchFields,
        },
      },
      'sort': {},
    }, {
      'fields': {
        'data': 'true',
      },
      'filter': {
        'data.searchRefDoc': {
          '$in': arrayOfSearchFields,
        },
      },
      'sort': {},
    },
    ];
    return searchCriteria;
  } catch (err) {
    handleErrors(true, true, 'BOT ERROR generateSearchParams =====================> ' + err);
  }
};

const localMatch = (arrayZero, arrayOne, arrayTwo) => {
  try {
    const responeObject = {};
    const arrayOfConfidenceMatches = [];
    const arrayOfPossibleMatches = [];
    let hasMatch = false;

    let combinedUniqeArrayOfAsnRecords = [];
    combinedUniqeArrayOfAsnRecords = _.unionBy(arrayZero, arrayOne, arrayTwo, 'db_id');

    if (combinedUniqeArrayOfAsnRecords.length == 0) {
      return {};
    }

    for (const asnRecord of combinedUniqeArrayOfAsnRecords) {
      let bolValue = {};
      let refDoc = {};
      let externalRef = {};
      const extractionObject = {}; // Added for capturing extracted values(12-05-2019)
      for (const extractedValue of arrayOfSearchFields) {
        bolValue = compareExtractedValues(extractedValue, asnRecord.data.searchBolNumber);
        refDoc = compareExtractedValues(extractedValue, asnRecord.data.searchRefDoc);
        externalRef = compareExtractedValues(extractedValue, asnRecord.data.searchExternalRef);
        // Below 3 lines will capture already exsisting values of bol,ref & extref
        const bolNo = asnRecord.bolProb;
        const refNo = asnRecord.refDocProb;
        const extRefNo = asnRecord.externalRefProb;
        asnRecord.bolProb = updateMatchProbForAsnRecord(bolValue, asnRecord.bolProb);
        // Below condition is for matching if existing bol object is same with current bol object.if not then updating the extracted bol same goes to ref & extRef down to the lines
        if (_.isMatch(bolNo, asnRecord.bolProb) == false) {
          extractionObject.extractedBolNumber = extractedValue;
        }

        asnRecord.refDocProb = updateMatchProbForAsnRecord(refDoc, asnRecord.refDocProb);
        if (_.isMatch(refNo, asnRecord.refDocProb) == false) {
          extractionObject.extractedRefDoc = extractedValue;
        }
        asnRecord.externalRefProb = updateMatchProbForAsnRecord(externalRef, asnRecord.externalRefProb);
        if (_.isMatch(extRefNo, asnRecord.externalRefProb) == false) {
          extractionObject.extractedExternalRef = extractedValue;
        }
      }
      asnRecord.extractionObject = extractionObject;
    }

    _.forEach(combinedUniqeArrayOfAsnRecords, ((currentAsnRecordWithMatch) => {
      let asnExactMatchCount = 0;
      let asnPartialMatchCount = 0;
      const hasExactBolMatch = _.get(currentAsnRecordWithMatch, 'bolProb.hasExactMatch', false);
      const hasRefDocMatch = _.get(currentAsnRecordWithMatch, 'externalRefProb.hasExactMatch', false);
      const hasExternalDocMatch = _.get(currentAsnRecordWithMatch, 'refDocProb.hasExactMatch', false);

      if (hasExactBolMatch) {
        asnExactMatchCount++;
      }
      if (hasRefDocMatch) {
        asnExactMatchCount++;
      }
      if (hasExternalDocMatch) {
        asnExactMatchCount++;
      }
      /* Brought all the partial and minimal match percent calculator conditions over here for generating both confidence and possible match arrays seamlessly after it.*/
      const hasPartialBolMatch = _.get(currentAsnRecordWithMatch, 'bolProb.subStringMatch', false);
      const hasPartialRefDocMatch = _.get(currentAsnRecordWithMatch, 'externalRefProb.subStringMatch', false);
      const hasPartialExternalDocMatch = _.get(currentAsnRecordWithMatch, 'refDocProb.subStringMatch', false);

      const hasMinimalBolMatch = _.get(currentAsnRecordWithMatch, 'bolProb.minimalMatch', false);
      const hasMinimalRefDocMatch = _.get(currentAsnRecordWithMatch, 'externalRefProb.minimalMatch', false);
      const hasMinimalExternalDocMatch = _.get(currentAsnRecordWithMatch, 'refDocProb.minimalMatch', false);

      if (hasPartialBolMatch || hasMinimalBolMatch) {
        if (hasPartialBolMatch) {
          asnPartialMatchCount = asnPartialMatchCount + currentAsnRecordWithMatch.bolProb.matchPrecent;
        } else if (hasMinimalBolMatch) {
          asnPartialMatchCount = asnPartialMatchCount + currentAsnRecordWithMatch.bolProb.matchPrecent;
        }
      }

      if (hasPartialRefDocMatch || hasMinimalRefDocMatch) {
        if (hasPartialRefDocMatch) {
          asnPartialMatchCount = asnPartialMatchCount + currentAsnRecordWithMatch.externalRefProb.matchPrecent;
        } else if (hasMinimalRefDocMatch) {
          asnPartialMatchCount = asnPartialMatchCount + currentAsnRecordWithMatch.externalRefProb.matchPrecent;
        }
      }

      if (hasPartialExternalDocMatch || hasMinimalExternalDocMatch) {
        if (hasPartialExternalDocMatch) {
          asnPartialMatchCount = asnPartialMatchCount + currentAsnRecordWithMatch.refDocProb.matchPrecent;
        } else if (hasMinimalExternalDocMatch) {
          asnPartialMatchCount = asnPartialMatchCount + currentAsnRecordWithMatch.refDocProb.matchPrecent;
        }
      }

      /* Added new conditions for Confident Match here*/
      //   if ((hasExactBolMatch && hasRefDocMatch && hasExternalDocMatch) || (hasExactBolMatch && hasExternalDocMatch) || (hasExactBolMatch && hasRefDocMatch) || hasExactBolMatch) {
      if ((currentAsnRecordWithMatch.data.billOfLading).length>6) {
        if ((hasExactBolMatch && hasRefDocMatch && hasExternalDocMatch) || (hasExactBolMatch && hasExternalDocMatch) || (hasExactBolMatch && hasRefDocMatch) || (hasExternalDocMatch && hasRefDocMatch) || (hasExactBolMatch)) {
          currentAsnRecordWithMatch.overAllScore = asnExactMatchCount + asnPartialMatchCount;
          currentAsnRecordWithMatch.percentMatch = (currentAsnRecordWithMatch.overAllScore / 3);
          arrayOfConfidenceMatches.push(currentAsnRecordWithMatch);
          hasMatch = true;
          return;
        }
      }


      const BolProbPercent = _.get(currentAsnRecordWithMatch, 'bolProb.matchPrecent', 0);
      const externalRefProbPercent = _.get(currentAsnRecordWithMatch, 'externalRefProb.matchPrecent', 0);
      const refDocProbPercent = _.get(currentAsnRecordWithMatch, 'refDocProb.matchPrecent', 0);
      // Added condition for matching  if BOL number is <6 digits and  satisfying other conditions as well
      // if ( ((hasExactBolMatch&&(currentAsnRecordWithMatch.data.billOfLading).length<6)&&externalRefProbPercent>=0.8)||((hasExactBolMatch&&(currentAsnRecordWithMatch.data.billOfLading).length<6)&&refDocProbPercent>=0.8)) {
      if ( ((hasExactBolMatch&&(currentAsnRecordWithMatch.data.billOfLading).length<=6)&&(hasExternalDocMatch||hasRefDocMatch))) {
        console.log('Inside the match criteria BOL less than length of 6');
        currentAsnRecordWithMatch.overAllScore = asnExactMatchCount + asnPartialMatchCount;
        currentAsnRecordWithMatch.percentMatch = (currentAsnRecordWithMatch.overAllScore / 3);
        arrayOfConfidenceMatches.push(currentAsnRecordWithMatch);
        hasMatch = true;
        return;
      }
      /* Added Conditions for less confident matches which requires User Intervention*/
      // if ((hasExternalDocMatch && hasRefDocMatch) || (BolProbPercent >= 0.8 && hasExternalDocMatch) || (BolProbPercent >= 0.8 && hasRefDocMatch)) {
      //   if ((externalRefProbPercent >= ExtRefThresHold && refDocProbPercent >= RefThresHold) || (BolProbPercent >= BolThresHold && externalRefProbPercent >= ExtRefThresHold) || (BolProbPercent >= BolThresHold && refDocProbPercent >= RefThresHold)) {
      if (((BolProbPercent>=BolThresHoldLowest&&BolProbPercent<=BolThresHoldHighest)&&(hasExternalDocMatch)&&(refDocProbPercent<RefThresHold))||((BolProbPercent>=BolThresHoldLowest&&BolProbPercent<=BolThresHoldHighest)&&(externalRefProbPercent<ExtRefThresHold)&&hasRefDocMatch)||(hasExactBolMatch&&(currentAsnRecordWithMatch.data.billOfLading).length<=6 && (externalRefProbPercent<ExtRefThresHold)&&(refDocProbPercent<RefThresHold))) {
        currentAsnRecordWithMatch.overAllScore = asnExactMatchCount + asnPartialMatchCount;
        currentAsnRecordWithMatch.percentMatch = (currentAsnRecordWithMatch.overAllScore / 3);
        arrayOfPossibleMatches.push(currentAsnRecordWithMatch);
        // hasMatch = true;
      }
    }));

    const orderedPossibleMatches = _.orderBy(arrayOfPossibleMatches, ['percentMatch'], ['desc']);

    const relevantPossibleMatches = _.slice(orderedPossibleMatches, [start = 0], [end = possibleMatchLimit]); // End can be configured as per user

    responeObject.hasMatch = hasMatch;
    responeObject.confidenceMatches = arrayOfConfidenceMatches;
    responeObject.lowConfidenceMatches = relevantPossibleMatches;
    return responeObject;
  } catch (err) {
    handleErrors(true, true, 'BOT ERROR localMatch =====================> ' + err);
  }
};

const updateMatchProbForAsnRecord = (probData, currentProb) => {
  try {
    const currentPropHasExactMatch = _.get(currentProb, 'hasExactMatch', false);
    const currentPropHasSubStringMatch = _.get(currentProb, 'subStringMatch', false);
    const newPropHasExactMatch = _.get(probData, 'hasExactMatch', false);
    const newPropHasSubStringMatch = _.get(probData, 'subStringMatch', false);
    const hasMinimalMatch = _.get(probData, 'minimalMatch', false);


    if (currentPropHasExactMatch) {
      return currentProb;
    } else if (newPropHasExactMatch) {
      return probData;
    } else if (newPropHasSubStringMatch && !currentPropHasSubStringMatch) {
      return probData;
    } else if (currentPropHasSubStringMatch && newPropHasSubStringMatch) {
      if (probData.matchPrecent > currentProb.matchPrecent) {
        return probData;
      } else {
        return currentProb;
      }
    } else if (hasMinimalMatch && !currentPropHasSubStringMatch && !currentPropHasExactMatch) {
      if ((probData.matchPrecent && !currentProb) || (probData.matchPrecent && currentProb && currentProb.matchPrecent && probData.matchPrecent > currentProb.matchPrecent)) {
        return probData;
      }
    } else {
      return currentProb;
    }
  } catch (err) {
    handleErrors(true, true, 'BOT ERROR updateMatchProbForAsnRecord =====================> ' + err);
  }
};

const compareExtractedValues = (extractedValue, asnRecordValue) => {
  try {
    if (extractedValue == asnRecordValue) {
      return ({
        hasExactMatch: true,
      });
    } else {
      const percentSimilarity = checkSimilarity(extractedValue, asnRecordValue);
      if (percentSimilarity > 0.7) {
        return {
          matchPrecent: percentSimilarity,
          subStringMatch: true,
        };
      } else if (percentSimilarity > 0.30) {
        return {
          matchPrecent: percentSimilarity,
          minimalMatch: true,
        };
      } else {
        return {};
      }
    }
  } catch (err) {
    handleErrors(true, true, 'BOT ERROR compareExtractedValues =====================> ' + err);
  }
};
/**
 * Check similarity between strings.
 * @param {string} extractedElement The first string.
 * @param {string} asnRecordElement The second string.
 * @return {similarity} The sum of the two numbers.
 */
function checkSimilarity(extractedElement, asnRecordElement) {
  const str1 = extractedElement;
  const str2 = asnRecordElement;
  return similarity(str1, str2);
}
/**
 * Return Similarity between two strings.
 * @param {string} s1 The first string.
 * @param {string} s2 The second string.
 * @return {similarity} The sum of the two numbers.
 */
function similarity(s1, s2) {
  let longer = s1;
  let shorter = s2;
  if (s1.length < s2.length) {
    longer = s2;
    shorter = s1;
  }
  const longerLength = longer.length;
  if (longerLength == 0) {
    return 1.0;
  }
  return (longerLength - editDistance(longer, shorter)) / parseFloat(longerLength);
}
/**
 * Checks distance between each characters and returns similarity.
 * @param {string} s1 The first string.
 * @param {string} s2 The second string.
 * @return {distance} The sum of the two numbers.
 */
function editDistance(s1, s2) {
  s1 = s1.toLowerCase();
  s2 = s2.toLowerCase();

  const costs = [];
  for (let i = 0; i <= s1.length; i++) {
    let lastValue = i;
    for (let j = 0; j <= s2.length; j++) {
      if (i == 0) {
        costs[j] = j;
      } else {
        if (j > 0) {
          let newValue = costs[j - 1];
          if (s1.charAt(i - 1) != s2.charAt(j - 1)) {
            newValue = Math.min(Math.min(newValue, lastValue),
                costs[j]) + 1;
          }
          costs[j - 1] = lastValue;
          lastValue = newValue;
        }
      }
    }
    if (i > 0) {
      costs[s2.length] = lastValue;
    }
  }
  return costs[s2.length];
}
const createResponse = (asnMatchingRecord, hasAsnMatch)=>{
  try {
    const cstmResponse={};
    let lowConfidenceMatches;
    if (!hasAsnMatch) {
      lowConfidenceMatches=asnMatchingRecord.localMatch.lowConfidenceMatches;
    }
    const confidentMatches=_.orderBy(_.unionBy(asnMatchingRecord.localMatch.confidenceMatches, asnMatchingRecord.dataSetMatch, 'db_id'), ['percentMatch'], ['desc']);
    // const confidentMatches=_.unionBy(asnMatchingRecord.localMatch.confidenceMatches, asnMatchingRecord.dataSetMatch, 'db_id');
    cstmResponse.hasConfidentMatch = hasAsnMatch;
    cstmResponse.confidentMatches = confidentMatches;
    cstmResponse.lowConfidenceMatches = lowConfidenceMatches;
    return cstmResponse;
  } catch (err) {
    handleErrors(true, true, 'BOT ERROR createResponse =====================> '+err);
  }
};
const mainBotExecution = async () => {
  try {
    // const responseObject = {
    //   data: {},
    // };
    const asnMatchingRecord = {};
    let hasAsnMatch = false;

    let extractedIdentifierValues = _.get(input, 'extractedData.values_by_types.Identifier', []);

    extractedIdentifierValues = extractedIdentifierValues.concat(_.get(input, 'extractedData.values_by_types.Number', []));

    rawOcrArray = input.extractedData.rawOcr.split(" ");

    const filteredOCR = _.filter(rawOcrArray, function(OCRstring) {
      return (OCRstring.length > 4) ? OCRstring : '';
    });
    const possibleBol = _.get(input, 'extractedData.boLNumber', 0);
    const possiblePo = _.get(input, 'extractedData.poNumber', 0);
    const possibleSeal = _.get(input, 'extractedData.sealNumber', 0);
    const additionalStrings = [];
    if (possibleBol != 0) additionalStrings.push(possibleBol);
    if (possiblePo != 0) additionalStrings.push(possiblePo);
    if (possibleSeal != 0) additionalStrings.push(possibleSeal);
    /* Changing OR to AND  here as to return below result both's length has to be 0*/
    if (extractedIdentifierValues.length < 1 && additionalStrings.length < 1 && filteredOCR.length < 1) {
      return {
        status: 'COMPLETED',
        data: {
          hasAsnMatch: hasAsnMatch,
          processMessage: 'No Data Extracted From Provided Document',
        },
      };
    }

    const searchParams = generateSearchParams(extractedIdentifierValues, additionalStrings, filteredOCR);

    const searchResults = await Promise.all(searchParams.map(async (searchParam) => {
      const result = await searchDataSetUsingQuery(searchParam, 'standard');
      result.query = searchParam;
      // console.log(JSON.stringify(searchParam));
      return result;
    }));

    // console.log(JSON.stringify(searchResults));

    const arrayZero = searchResults[0].content;
    const arrayOne = searchResults[1].content;
    const arrayTwo = searchResults[2].content;

    const valuesInAllArrays = _.intersectionWith(arrayZero, arrayOne, arrayTwo, _.isEqual);
    // Getting the differences after intersection
    const arrayZeroLocal = _.differenceWith(arrayZero, valuesInAllArrays, _.isEqual);
    const arrayOneLocal = _.differenceWith(arrayOne, valuesInAllArrays, _.isEqual);
    const arrayTwoLocal = _.differenceWith(arrayTwo, valuesInAllArrays, _.isEqual);

    if (valuesInAllArrays.length > 0) {
      const finalfinalvaluesInAllArrays = _.cloneDeep(valuesInAllArrays);
      finalfinalvaluesInAllArrays.map((item) => item.percentMatch = 1);
      asnMatchingRecord.dataSetMatch = finalfinalvaluesInAllArrays;
      hasAsnMatch = true;
    }
    // Passing the rest of the array items after intersection for local matching
    asnMatchingRecord.localMatch = localMatch(arrayZeroLocal, arrayOneLocal, arrayTwoLocal);
    if (asnMatchingRecord.localMatch.hasMatch == true) {
      hasAsnMatch = true;
      delete asnMatchingRecord.localMatch.hasMatch;
    } else if (!hasAsnMatch) {
      response.data.processMessage = 'No ASN Records Found Based on Search Criteria';
      delete asnMatchingRecord.localMatch.hasMatch;
    }
    const responseData = createResponse(asnMatchingRecord, hasAsnMatch);
    // response.data = { };
    // response.data = responseData; 
    response.data.hasConfidentMatch = responseData.hasConfidentMatch;
    response.data.confidentMatches = responseData.confidentMatches;
    response.data.lowConfidenceMatches = responseData.lowConfidenceMatches;
    response.status = 'COMPLETED';
    //COMPLETED, IN_PROGRESS, FAILED, NEED_REVIEW

    // let fs = require('fs');
    //fs.writeFileSync('./response.json', JSON.stringify(responseData, null, 2));
    console.log(JSON.stringify(response, null, 2));
    return response;
  } catch (err) {
    handleErrors(true, true, 'BOT ERROR mainBotExecution =====================> ' + err);
  }
};
return mainBotExecution();

